#include "realistic_movement_strategy.h"
#include "config.h"

using namespace strategy;
using geometry::Vector2D;

Vector2D RealisticMovementStrategy::nextPosition(Vector2D currentPosition, Vector2D currentVelocity) {
    return currentPosition.summedWith(currentVelocity);
}

Vector2D RealisticMovementStrategy::nextVelocity(Vector2D currentVelocity) {
    return currentVelocity.summedWith(Vector2D::UNIT_DOWN.multipliedBy(Config::get().getGravity()));
}

RealisticMovementStrategy * RealisticMovementStrategy::copy() const {
    return new RealisticMovementStrategy();
}
