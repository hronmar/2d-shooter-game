#ifndef ENEMY_H
#define ENEMY_H

#include "game_object.h"

namespace model {

    class Enemy : public GameObject {
    public:

        virtual ~Enemy() {
        }

        virtual void move() = 0;

        void accept(visitor::Visitor& visitor) override;

        virtual Enemy * copy() const = 0;
    };

}

#endif /* ENEMY_H */
