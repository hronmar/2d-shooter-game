#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
CND_BUILDDIR=build
CND_DISTDIR=dist
# Debug configuration
CND_PLATFORM_Debug=GNU-Linux
CND_ARTIFACT_DIR_Debug=dist/Debug/GNU-Linux
CND_ARTIFACT_NAME_Debug=mi-adp-semestral-work
CND_ARTIFACT_PATH_Debug=dist/Debug/GNU-Linux/mi-adp-semestral-work
CND_PACKAGE_DIR_Debug=dist/Debug/GNU-Linux/package
CND_PACKAGE_NAME_Debug=mi-adp-semestral-work.tar
CND_PACKAGE_PATH_Debug=dist/Debug/GNU-Linux/package/mi-adp-semestral-work.tar
# Release configuration
CND_PLATFORM_Release=GNU-Linux
CND_ARTIFACT_DIR_Release=dist/Release/GNU-Linux
CND_ARTIFACT_NAME_Release=mi-adp-semestral-work
CND_ARTIFACT_PATH_Release=dist/Release/GNU-Linux/mi-adp-semestral-work
CND_PACKAGE_DIR_Release=dist/Release/GNU-Linux/package
CND_PACKAGE_NAME_Release=mi-adp-semestral-work.tar
CND_PACKAGE_PATH_Release=dist/Release/GNU-Linux/package/mi-adp-semestral-work.tar
# Valgrind configuration
CND_PLATFORM_Valgrind=GNU-Linux
CND_ARTIFACT_DIR_Valgrind=dist/Valgrind/GNU-Linux
CND_ARTIFACT_NAME_Valgrind=mi-adp-semestral-work
CND_ARTIFACT_PATH_Valgrind=dist/Valgrind/GNU-Linux/mi-adp-semestral-work
CND_PACKAGE_DIR_Valgrind=dist/Valgrind/GNU-Linux/package
CND_PACKAGE_NAME_Valgrind=mi-adp-semestral-work.tar
CND_PACKAGE_PATH_Valgrind=dist/Valgrind/GNU-Linux/package/mi-adp-semestral-work.tar
#
# include compiler specific variables
#
# dmake command
ROOT:sh = test -f nbproject/private/Makefile-variables.mk || \
	(mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk)
#
# gmake command
.PHONY: $(shell test -f nbproject/private/Makefile-variables.mk || (mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk))
#
include nbproject/private/Makefile-variables.mk
