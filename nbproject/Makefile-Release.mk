#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-Linux
CND_DLIB_EXT=so
CND_CONF=Release
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/abstract_game_model.o \
	${OBJECTDIR}/cannon.o \
	${OBJECTDIR}/cannon_shoot_event_handler.o \
	${OBJECTDIR}/collision.o \
	${OBJECTDIR}/composite_event_handler.o \
	${OBJECTDIR}/double_shooting_mode.o \
	${OBJECTDIR}/enemy.o \
	${OBJECTDIR}/enemy_die_event_handler.o \
	${OBJECTDIR}/event_emitter.o \
	${OBJECTDIR}/game.o \
	${OBJECTDIR}/game_command.o \
	${OBJECTDIR}/game_controller.o \
	${OBJECTDIR}/game_model.o \
	${OBJECTDIR}/game_model_proxy.o \
	${OBJECTDIR}/game_window.o \
	${OBJECTDIR}/main.o \
	${OBJECTDIR}/method_call_command.o \
	${OBJECTDIR}/missile.o \
	${OBJECTDIR}/missile_hit_event_handler.o \
	${OBJECTDIR}/objects_drawer.o \
	${OBJECTDIR}/realistic_enemy.o \
	${OBJECTDIR}/realistic_game_objects_factory.o \
	${OBJECTDIR}/realistic_movement_strategy.o \
	${OBJECTDIR}/rectangle.o \
	${OBJECTDIR}/resource_loader.o \
	${OBJECTDIR}/simple_enemy.o \
	${OBJECTDIR}/simple_game_objects_factory.o \
	${OBJECTDIR}/simple_movement_strategy.o \
	${OBJECTDIR}/single_shooting_mode.o \
	${OBJECTDIR}/subject.o \
	${OBJECTDIR}/vector_2D.o

# Test Directory
TESTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}/tests

# Test Files
TESTFILES= \
	${TESTDIR}/TestFiles/f1 \
	${TESTDIR}/TestFiles/f2 \
	${TESTDIR}/TestFiles/f3

# Test Object Files
TESTOBJECTFILES= \
	${TESTDIR}/tests/game_model_testrunner.o \
	${TESTDIR}/tests/game_model_tests.o \
	${TESTDIR}/tests/geometry_testrunner.o \
	${TESTDIR}/tests/geometry_tests.o \
	${TESTDIR}/tests/observer_testrunner.o \
	${TESTDIR}/tests/observer_tests.o

# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=`pkg-config --libs sfml-all`  

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/mi-adp-semestral-work

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/mi-adp-semestral-work: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/mi-adp-semestral-work ${OBJECTFILES} ${LDLIBSOPTIONS}

${OBJECTDIR}/abstract_game_model.o: abstract_game_model.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 `pkg-config --cflags sfml-all` -std=c++14  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/abstract_game_model.o abstract_game_model.cpp

${OBJECTDIR}/cannon.o: cannon.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 `pkg-config --cflags sfml-all` -std=c++14  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/cannon.o cannon.cpp

${OBJECTDIR}/cannon_shoot_event_handler.o: cannon_shoot_event_handler.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 `pkg-config --cflags sfml-all` -std=c++14  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/cannon_shoot_event_handler.o cannon_shoot_event_handler.cpp

${OBJECTDIR}/collision.o: collision.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 `pkg-config --cflags sfml-all` -std=c++14  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/collision.o collision.cpp

${OBJECTDIR}/composite_event_handler.o: composite_event_handler.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 `pkg-config --cflags sfml-all` -std=c++14  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/composite_event_handler.o composite_event_handler.cpp

${OBJECTDIR}/double_shooting_mode.o: double_shooting_mode.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 `pkg-config --cflags sfml-all` -std=c++14  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/double_shooting_mode.o double_shooting_mode.cpp

${OBJECTDIR}/enemy.o: enemy.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 `pkg-config --cflags sfml-all` -std=c++14  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/enemy.o enemy.cpp

${OBJECTDIR}/enemy_die_event_handler.o: enemy_die_event_handler.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 `pkg-config --cflags sfml-all` -std=c++14  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/enemy_die_event_handler.o enemy_die_event_handler.cpp

${OBJECTDIR}/event_emitter.o: event_emitter.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 `pkg-config --cflags sfml-all` -std=c++14  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/event_emitter.o event_emitter.cpp

${OBJECTDIR}/game.o: game.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 `pkg-config --cflags sfml-all` -std=c++14  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/game.o game.cpp

${OBJECTDIR}/game_command.o: game_command.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 `pkg-config --cflags sfml-all` -std=c++14  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/game_command.o game_command.cpp

${OBJECTDIR}/game_controller.o: game_controller.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 `pkg-config --cflags sfml-all` -std=c++14  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/game_controller.o game_controller.cpp

${OBJECTDIR}/game_model.o: game_model.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 `pkg-config --cflags sfml-all` -std=c++14  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/game_model.o game_model.cpp

${OBJECTDIR}/game_model_proxy.o: game_model_proxy.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 `pkg-config --cflags sfml-all` -std=c++14  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/game_model_proxy.o game_model_proxy.cpp

${OBJECTDIR}/game_window.o: game_window.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 `pkg-config --cflags sfml-all` -std=c++14  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/game_window.o game_window.cpp

${OBJECTDIR}/main.o: main.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 `pkg-config --cflags sfml-all` -std=c++14  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/main.o main.cpp

${OBJECTDIR}/method_call_command.o: method_call_command.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 `pkg-config --cflags sfml-all` -std=c++14  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/method_call_command.o method_call_command.cpp

${OBJECTDIR}/missile.o: missile.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 `pkg-config --cflags sfml-all` -std=c++14  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/missile.o missile.cpp

${OBJECTDIR}/missile_hit_event_handler.o: missile_hit_event_handler.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 `pkg-config --cflags sfml-all` -std=c++14  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/missile_hit_event_handler.o missile_hit_event_handler.cpp

${OBJECTDIR}/objects_drawer.o: objects_drawer.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 `pkg-config --cflags sfml-all` -std=c++14  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/objects_drawer.o objects_drawer.cpp

${OBJECTDIR}/realistic_enemy.o: realistic_enemy.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 `pkg-config --cflags sfml-all` -std=c++14  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/realistic_enemy.o realistic_enemy.cpp

${OBJECTDIR}/realistic_game_objects_factory.o: realistic_game_objects_factory.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 `pkg-config --cflags sfml-all` -std=c++14  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/realistic_game_objects_factory.o realistic_game_objects_factory.cpp

${OBJECTDIR}/realistic_movement_strategy.o: realistic_movement_strategy.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 `pkg-config --cflags sfml-all` -std=c++14  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/realistic_movement_strategy.o realistic_movement_strategy.cpp

${OBJECTDIR}/rectangle.o: rectangle.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 `pkg-config --cflags sfml-all` -std=c++14  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/rectangle.o rectangle.cpp

${OBJECTDIR}/resource_loader.o: resource_loader.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 `pkg-config --cflags sfml-all` -std=c++14  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/resource_loader.o resource_loader.cpp

${OBJECTDIR}/simple_enemy.o: simple_enemy.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 `pkg-config --cflags sfml-all` -std=c++14  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/simple_enemy.o simple_enemy.cpp

${OBJECTDIR}/simple_game_objects_factory.o: simple_game_objects_factory.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 `pkg-config --cflags sfml-all` -std=c++14  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/simple_game_objects_factory.o simple_game_objects_factory.cpp

${OBJECTDIR}/simple_movement_strategy.o: simple_movement_strategy.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 `pkg-config --cflags sfml-all` -std=c++14  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/simple_movement_strategy.o simple_movement_strategy.cpp

${OBJECTDIR}/single_shooting_mode.o: single_shooting_mode.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 `pkg-config --cflags sfml-all` -std=c++14  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/single_shooting_mode.o single_shooting_mode.cpp

${OBJECTDIR}/subject.o: subject.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 `pkg-config --cflags sfml-all` -std=c++14  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/subject.o subject.cpp

${OBJECTDIR}/vector_2D.o: vector_2D.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 `pkg-config --cflags sfml-all` -std=c++14  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/vector_2D.o vector_2D.cpp

# Subprojects
.build-subprojects:

# Build Test Targets
.build-tests-conf: .build-tests-subprojects .build-conf ${TESTFILES}
.build-tests-subprojects:

${TESTDIR}/TestFiles/f1: ${TESTDIR}/tests/game_model_testrunner.o ${TESTDIR}/tests/game_model_tests.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${TESTDIR}/TestFiles
	${LINK.cc} -o ${TESTDIR}/TestFiles/f1 $^ ${LDLIBSOPTIONS}   

${TESTDIR}/TestFiles/f2: ${TESTDIR}/tests/geometry_testrunner.o ${TESTDIR}/tests/geometry_tests.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${TESTDIR}/TestFiles
	${LINK.cc} -o ${TESTDIR}/TestFiles/f2 $^ ${LDLIBSOPTIONS}   `cppunit-config --libs`   

${TESTDIR}/TestFiles/f3: ${TESTDIR}/tests/observer_testrunner.o ${TESTDIR}/tests/observer_tests.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${TESTDIR}/TestFiles
	${LINK.cc} -o ${TESTDIR}/TestFiles/f3 $^ ${LDLIBSOPTIONS}   `cppunit-config --libs`   


${TESTDIR}/tests/game_model_testrunner.o: tests/game_model_testrunner.cpp 
	${MKDIR} -p ${TESTDIR}/tests
	${RM} "$@.d"
	$(COMPILE.cc) -O2 `pkg-config --cflags sfml-all` -std=c++14  -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/game_model_testrunner.o tests/game_model_testrunner.cpp


${TESTDIR}/tests/game_model_tests.o: tests/game_model_tests.cpp 
	${MKDIR} -p ${TESTDIR}/tests
	${RM} "$@.d"
	$(COMPILE.cc) -O2 `pkg-config --cflags sfml-all` -std=c++14  -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/game_model_tests.o tests/game_model_tests.cpp


${TESTDIR}/tests/geometry_testrunner.o: tests/geometry_testrunner.cpp 
	${MKDIR} -p ${TESTDIR}/tests
	${RM} "$@.d"
	$(COMPILE.cc) -O2 `pkg-config --cflags sfml-all` -std=c++14 `cppunit-config --cflags` -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/geometry_testrunner.o tests/geometry_testrunner.cpp


${TESTDIR}/tests/geometry_tests.o: tests/geometry_tests.cpp 
	${MKDIR} -p ${TESTDIR}/tests
	${RM} "$@.d"
	$(COMPILE.cc) -O2 `pkg-config --cflags sfml-all` -std=c++14 `cppunit-config --cflags` -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/geometry_tests.o tests/geometry_tests.cpp


${TESTDIR}/tests/observer_testrunner.o: tests/observer_testrunner.cpp 
	${MKDIR} -p ${TESTDIR}/tests
	${RM} "$@.d"
	$(COMPILE.cc) -O2 `pkg-config --cflags sfml-all` -std=c++14 `cppunit-config --cflags` -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/observer_testrunner.o tests/observer_testrunner.cpp


${TESTDIR}/tests/observer_tests.o: tests/observer_tests.cpp 
	${MKDIR} -p ${TESTDIR}/tests
	${RM} "$@.d"
	$(COMPILE.cc) -O2 `pkg-config --cflags sfml-all` -std=c++14 `cppunit-config --cflags` -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/observer_tests.o tests/observer_tests.cpp


${OBJECTDIR}/abstract_game_model_nomain.o: ${OBJECTDIR}/abstract_game_model.o abstract_game_model.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/abstract_game_model.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -O2 `pkg-config --cflags sfml-all` -std=c++14  -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/abstract_game_model_nomain.o abstract_game_model.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/abstract_game_model.o ${OBJECTDIR}/abstract_game_model_nomain.o;\
	fi

${OBJECTDIR}/cannon_nomain.o: ${OBJECTDIR}/cannon.o cannon.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/cannon.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -O2 `pkg-config --cflags sfml-all` -std=c++14  -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/cannon_nomain.o cannon.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/cannon.o ${OBJECTDIR}/cannon_nomain.o;\
	fi

${OBJECTDIR}/cannon_shoot_event_handler_nomain.o: ${OBJECTDIR}/cannon_shoot_event_handler.o cannon_shoot_event_handler.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/cannon_shoot_event_handler.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -O2 `pkg-config --cflags sfml-all` -std=c++14  -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/cannon_shoot_event_handler_nomain.o cannon_shoot_event_handler.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/cannon_shoot_event_handler.o ${OBJECTDIR}/cannon_shoot_event_handler_nomain.o;\
	fi

${OBJECTDIR}/collision_nomain.o: ${OBJECTDIR}/collision.o collision.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/collision.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -O2 `pkg-config --cflags sfml-all` -std=c++14  -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/collision_nomain.o collision.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/collision.o ${OBJECTDIR}/collision_nomain.o;\
	fi

${OBJECTDIR}/composite_event_handler_nomain.o: ${OBJECTDIR}/composite_event_handler.o composite_event_handler.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/composite_event_handler.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -O2 `pkg-config --cflags sfml-all` -std=c++14  -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/composite_event_handler_nomain.o composite_event_handler.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/composite_event_handler.o ${OBJECTDIR}/composite_event_handler_nomain.o;\
	fi

${OBJECTDIR}/double_shooting_mode_nomain.o: ${OBJECTDIR}/double_shooting_mode.o double_shooting_mode.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/double_shooting_mode.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -O2 `pkg-config --cflags sfml-all` -std=c++14  -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/double_shooting_mode_nomain.o double_shooting_mode.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/double_shooting_mode.o ${OBJECTDIR}/double_shooting_mode_nomain.o;\
	fi

${OBJECTDIR}/enemy_nomain.o: ${OBJECTDIR}/enemy.o enemy.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/enemy.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -O2 `pkg-config --cflags sfml-all` -std=c++14  -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/enemy_nomain.o enemy.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/enemy.o ${OBJECTDIR}/enemy_nomain.o;\
	fi

${OBJECTDIR}/enemy_die_event_handler_nomain.o: ${OBJECTDIR}/enemy_die_event_handler.o enemy_die_event_handler.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/enemy_die_event_handler.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -O2 `pkg-config --cflags sfml-all` -std=c++14  -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/enemy_die_event_handler_nomain.o enemy_die_event_handler.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/enemy_die_event_handler.o ${OBJECTDIR}/enemy_die_event_handler_nomain.o;\
	fi

${OBJECTDIR}/event_emitter_nomain.o: ${OBJECTDIR}/event_emitter.o event_emitter.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/event_emitter.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -O2 `pkg-config --cflags sfml-all` -std=c++14  -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/event_emitter_nomain.o event_emitter.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/event_emitter.o ${OBJECTDIR}/event_emitter_nomain.o;\
	fi

${OBJECTDIR}/game_nomain.o: ${OBJECTDIR}/game.o game.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/game.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -O2 `pkg-config --cflags sfml-all` -std=c++14  -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/game_nomain.o game.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/game.o ${OBJECTDIR}/game_nomain.o;\
	fi

${OBJECTDIR}/game_command_nomain.o: ${OBJECTDIR}/game_command.o game_command.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/game_command.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -O2 `pkg-config --cflags sfml-all` -std=c++14  -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/game_command_nomain.o game_command.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/game_command.o ${OBJECTDIR}/game_command_nomain.o;\
	fi

${OBJECTDIR}/game_controller_nomain.o: ${OBJECTDIR}/game_controller.o game_controller.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/game_controller.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -O2 `pkg-config --cflags sfml-all` -std=c++14  -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/game_controller_nomain.o game_controller.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/game_controller.o ${OBJECTDIR}/game_controller_nomain.o;\
	fi

${OBJECTDIR}/game_model_nomain.o: ${OBJECTDIR}/game_model.o game_model.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/game_model.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -O2 `pkg-config --cflags sfml-all` -std=c++14  -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/game_model_nomain.o game_model.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/game_model.o ${OBJECTDIR}/game_model_nomain.o;\
	fi

${OBJECTDIR}/game_model_proxy_nomain.o: ${OBJECTDIR}/game_model_proxy.o game_model_proxy.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/game_model_proxy.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -O2 `pkg-config --cflags sfml-all` -std=c++14  -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/game_model_proxy_nomain.o game_model_proxy.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/game_model_proxy.o ${OBJECTDIR}/game_model_proxy_nomain.o;\
	fi

${OBJECTDIR}/game_window_nomain.o: ${OBJECTDIR}/game_window.o game_window.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/game_window.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -O2 `pkg-config --cflags sfml-all` -std=c++14  -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/game_window_nomain.o game_window.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/game_window.o ${OBJECTDIR}/game_window_nomain.o;\
	fi

${OBJECTDIR}/main_nomain.o: ${OBJECTDIR}/main.o main.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/main.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -O2 `pkg-config --cflags sfml-all` -std=c++14  -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/main_nomain.o main.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/main.o ${OBJECTDIR}/main_nomain.o;\
	fi

${OBJECTDIR}/method_call_command_nomain.o: ${OBJECTDIR}/method_call_command.o method_call_command.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/method_call_command.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -O2 `pkg-config --cflags sfml-all` -std=c++14  -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/method_call_command_nomain.o method_call_command.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/method_call_command.o ${OBJECTDIR}/method_call_command_nomain.o;\
	fi

${OBJECTDIR}/missile_nomain.o: ${OBJECTDIR}/missile.o missile.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/missile.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -O2 `pkg-config --cflags sfml-all` -std=c++14  -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/missile_nomain.o missile.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/missile.o ${OBJECTDIR}/missile_nomain.o;\
	fi

${OBJECTDIR}/missile_hit_event_handler_nomain.o: ${OBJECTDIR}/missile_hit_event_handler.o missile_hit_event_handler.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/missile_hit_event_handler.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -O2 `pkg-config --cflags sfml-all` -std=c++14  -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/missile_hit_event_handler_nomain.o missile_hit_event_handler.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/missile_hit_event_handler.o ${OBJECTDIR}/missile_hit_event_handler_nomain.o;\
	fi

${OBJECTDIR}/objects_drawer_nomain.o: ${OBJECTDIR}/objects_drawer.o objects_drawer.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/objects_drawer.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -O2 `pkg-config --cflags sfml-all` -std=c++14  -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/objects_drawer_nomain.o objects_drawer.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/objects_drawer.o ${OBJECTDIR}/objects_drawer_nomain.o;\
	fi

${OBJECTDIR}/realistic_enemy_nomain.o: ${OBJECTDIR}/realistic_enemy.o realistic_enemy.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/realistic_enemy.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -O2 `pkg-config --cflags sfml-all` -std=c++14  -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/realistic_enemy_nomain.o realistic_enemy.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/realistic_enemy.o ${OBJECTDIR}/realistic_enemy_nomain.o;\
	fi

${OBJECTDIR}/realistic_game_objects_factory_nomain.o: ${OBJECTDIR}/realistic_game_objects_factory.o realistic_game_objects_factory.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/realistic_game_objects_factory.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -O2 `pkg-config --cflags sfml-all` -std=c++14  -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/realistic_game_objects_factory_nomain.o realistic_game_objects_factory.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/realistic_game_objects_factory.o ${OBJECTDIR}/realistic_game_objects_factory_nomain.o;\
	fi

${OBJECTDIR}/realistic_movement_strategy_nomain.o: ${OBJECTDIR}/realistic_movement_strategy.o realistic_movement_strategy.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/realistic_movement_strategy.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -O2 `pkg-config --cflags sfml-all` -std=c++14  -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/realistic_movement_strategy_nomain.o realistic_movement_strategy.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/realistic_movement_strategy.o ${OBJECTDIR}/realistic_movement_strategy_nomain.o;\
	fi

${OBJECTDIR}/rectangle_nomain.o: ${OBJECTDIR}/rectangle.o rectangle.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/rectangle.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -O2 `pkg-config --cflags sfml-all` -std=c++14  -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/rectangle_nomain.o rectangle.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/rectangle.o ${OBJECTDIR}/rectangle_nomain.o;\
	fi

${OBJECTDIR}/resource_loader_nomain.o: ${OBJECTDIR}/resource_loader.o resource_loader.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/resource_loader.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -O2 `pkg-config --cflags sfml-all` -std=c++14  -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/resource_loader_nomain.o resource_loader.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/resource_loader.o ${OBJECTDIR}/resource_loader_nomain.o;\
	fi

${OBJECTDIR}/simple_enemy_nomain.o: ${OBJECTDIR}/simple_enemy.o simple_enemy.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/simple_enemy.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -O2 `pkg-config --cflags sfml-all` -std=c++14  -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/simple_enemy_nomain.o simple_enemy.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/simple_enemy.o ${OBJECTDIR}/simple_enemy_nomain.o;\
	fi

${OBJECTDIR}/simple_game_objects_factory_nomain.o: ${OBJECTDIR}/simple_game_objects_factory.o simple_game_objects_factory.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/simple_game_objects_factory.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -O2 `pkg-config --cflags sfml-all` -std=c++14  -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/simple_game_objects_factory_nomain.o simple_game_objects_factory.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/simple_game_objects_factory.o ${OBJECTDIR}/simple_game_objects_factory_nomain.o;\
	fi

${OBJECTDIR}/simple_movement_strategy_nomain.o: ${OBJECTDIR}/simple_movement_strategy.o simple_movement_strategy.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/simple_movement_strategy.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -O2 `pkg-config --cflags sfml-all` -std=c++14  -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/simple_movement_strategy_nomain.o simple_movement_strategy.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/simple_movement_strategy.o ${OBJECTDIR}/simple_movement_strategy_nomain.o;\
	fi

${OBJECTDIR}/single_shooting_mode_nomain.o: ${OBJECTDIR}/single_shooting_mode.o single_shooting_mode.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/single_shooting_mode.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -O2 `pkg-config --cflags sfml-all` -std=c++14  -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/single_shooting_mode_nomain.o single_shooting_mode.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/single_shooting_mode.o ${OBJECTDIR}/single_shooting_mode_nomain.o;\
	fi

${OBJECTDIR}/subject_nomain.o: ${OBJECTDIR}/subject.o subject.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/subject.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -O2 `pkg-config --cflags sfml-all` -std=c++14  -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/subject_nomain.o subject.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/subject.o ${OBJECTDIR}/subject_nomain.o;\
	fi

${OBJECTDIR}/vector_2D_nomain.o: ${OBJECTDIR}/vector_2D.o vector_2D.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/vector_2D.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -O2 `pkg-config --cflags sfml-all` -std=c++14  -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/vector_2D_nomain.o vector_2D.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/vector_2D.o ${OBJECTDIR}/vector_2D_nomain.o;\
	fi

# Run Test Targets
.test-conf:
	@if [ "${TEST}" = "" ]; \
	then  \
	    ${TESTDIR}/TestFiles/f1 || true; \
	    ${TESTDIR}/TestFiles/f2 || true; \
	    ${TESTDIR}/TestFiles/f3 || true; \
	else  \
	    ./${TEST} || true; \
	fi

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
