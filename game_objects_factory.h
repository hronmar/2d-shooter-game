#ifndef GAME_OBJECTS_FACTORY_H
#define GAME_OBJECTS_FACTORY_H

#include "cannon.h"
#include "collision.h"
#include "missile.h"
#include "enemy.h"

using namespace model;
using geometry::Vector2D;

namespace abstract_factory {

    /* Abstract game objects factory */
    class GameObjectsFactory {
    public:

        virtual ~GameObjectsFactory() {
        }

        virtual Cannon * createCannon() = 0;
        virtual Collision * createCollision(Vector2D position) = 0;
        virtual Enemy * createEnemy(Vector2D position) = 0;
        virtual Missile * createMissile(Vector2D position, Vector2D velocity) = 0;
    };

}

#endif /* GAME_OBJECTS_FACTORY_H */
