#ifndef GAME_OBJECT_H
#define GAME_OBJECT_H

#include <SFML/System/Vector2.hpp>

#include "vector_2D.h"
#include "rectangle.h"

//forward declaration
namespace visitor {
    class Visitor;
}

namespace model {

    /* An abstract game object */
    class GameObject {
    public:

        virtual ~GameObject() {
        }

        const geometry::Vector2D & getPosition() const {
            return m_Position;
        }

        geometry::Vector2D & getPosition() {
            return m_Position;
        }

        void setPosition(geometry::Vector2D position) {
            m_Position = position;
        }

        virtual geometry::Vector2D getSize() const {
            return geometry::Vector2D::ZERO;
        }

        geometry::Rectangle getBoundingBox() const {
            return geometry::Rectangle(m_Position, getSize());
        }
        
        bool collidesWith(GameObject * o) const {
            return getBoundingBox().intersectsWith(o->getBoundingBox());
        }

        virtual void accept(visitor::Visitor & visitor) = 0;

        virtual GameObject * copy() const = 0;

    protected:
        geometry::Vector2D m_Position;
    };

}

#endif /* GAME_OBJECT_H */
