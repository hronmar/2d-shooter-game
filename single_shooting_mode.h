#ifndef SINGLE_SHOOTING_MODE_H
#define SINGLE_SHOOTING_MODE_H

#include "cannon_shooting_mode.h"

namespace model {
    class Cannon;
}

namespace state {

    class SingleShootingMode : public CannonShootingMode {
    public:
        SingleShootingMode(model::Cannon * context);

        void shoot() override;
        void toggleMode() override;
        std::string printString() const override;

        SingleShootingMode * newInContext(model::Cannon * context) const override;
    };

}

#endif /* SINGLE_SHOOTING_MODE_H */
