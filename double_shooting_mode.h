#ifndef DOUBLE_SHOOTING_MODE_H
#define DOUBLE_SHOOTING_MODE_H

#include "cannon_shooting_mode.h"

namespace model {
    class Cannon;
}

namespace state {

    class DoubleShootingMode : public CannonShootingMode {
    public:
        DoubleShootingMode(model::Cannon * context);

        void shoot() override;
        void toggleMode() override;
        std::string printString() const override;
        
        DoubleShootingMode * newInContext(model::Cannon * context) const override;
    };

}

#endif /* DOUBLE_SHOOTING_MODE_H */
