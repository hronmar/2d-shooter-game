#ifndef SIMPLE_MOVEMENT_STRATEGY_H
#define SIMPLE_MOVEMENT_STRATEGY_H

#include "movement_strategy.h"

using geometry::Vector2D;

namespace strategy {

    /* Simple movement strategy using a line movement */
    class SimpleMovementStrategy : public MovementStrategy {
    public:

        Vector2D nextPosition(Vector2D currentPosition, Vector2D currentVelocity) override;
        Vector2D nextVelocity(Vector2D currentVelocity) override;

        SimpleMovementStrategy * copy() const override;
    };

}

#endif /* SIMPLE_MOVEMENT_STRATEGY_H */
