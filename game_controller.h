#ifndef GAME_CONTROLLER_H
#define GAME_CONTROLLER_H

#include <SFML/Window.hpp>

#include "abstract_game_model.h"

using proxy::AbstractGameModel;

namespace controller {

    /* Controller in the MVC pattern. 
     * Handles events caused by user interaction and invokes model methods. */
    class GameController {
    public:
        explicit GameController(AbstractGameModel * model);

        void processEvent(sf::Event event);

        AbstractGameModel * getModel() const;

    private:
        AbstractGameModel * m_Model;
    };

}

#endif /* GAME_CONTROLLER_H */
