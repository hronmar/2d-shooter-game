#ifndef MISSILE_HIT_EVENT_HANDLER_H
#define MISSILE_HIT_EVENT_HANDLER_H

#include <SFML/Audio/Sound.hpp>

#include "event_handler.h"
#include "resource_loader.h"

namespace events {

    class MissileHitEventHandler : public EventHandler {
    public:

        MissileHitEventHandler(EventHandler * nextInChain, flyweight::ResourceLoader * resourceHolder);
        MissileHitEventHandler(flyweight::ResourceLoader * resourceHolder);

        void handleEvent(GameEvent event) override;

    private:
        flyweight::ResourceLoader * m_ResourceLoader;

        sf::Sound m_Sound;
        static const std::string HIT_SOUND;
    };

}

#endif /* MISSILE_HIT_EVENT_HANDLER_H */
