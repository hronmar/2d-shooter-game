#ifndef GAME_MODEL_H
#define GAME_MODEL_H

#include <list>
#include <memory>
#include <queue>
#include <vector>

#include "subject.h"
#include "game_object.h"
#include "cannon.h"
#include "enemy.h"
#include "missile.h"
#include "game_objects_factory.h"
#include "abstract_game_model.h"

using abstract_factory::GameObjectsFactory;
using proxy::AbstractGameModel;
using command::GameCommand;
using geometry::Vector2D;

namespace model {

    class GameModel;

    /* Used for storing model's internal state */
    class Memento {
    public:
        virtual ~Memento();
    private:
        friend class GameModel;
        int m_Score;
        std::vector<Missile *> m_Missiles;
        std::vector<Enemy *> m_Enemies;
        std::vector<Collision *> m_Collisions;
        Cannon * m_Cannon;
    };

    /* The model in MVC. 
     * Calling the tick() member function executes one simulation frame. */
    class GameModel : public AbstractGameModel {
    public:
        GameModel(GameObjectsFactory * factory);
        ~GameModel();

        void tick();

        const std::list<GameObject *> & getGameObjects() const;

        int getScore() const override;
        std::string getTextInfo() const;
        model::Cannon * getCannon() const override;

        void moveCannonUp();
        void moveCannonDown();
        void aimCannonUp();
        void aimCannonDown();
        void increaseForce();
        void decreaseForce();
        void shootCannon();
        void toggleShootingMode();

        bool isGameEnd() const override;
        void reset() override;

        void registerCommand(GameCommand * command) override;
        void undoLastCommand() override;

        /* Creates a memento encapsulating current model state */
        Memento * createMemento() override;

        /* Restores model state from given object. This method takes ownership 
         * of the pointed to memory, the object should NOT be used again 
         * after passed to this function. */
        void setMemento(Memento * memento) override;

    private:
        GameObjectsFactory * m_Factory;
        std::list<GameObject *> m_GameObjects;
        std::list<Missile *> m_Missiles;
        std::list<Enemy *> m_Enemies;
        std::list<Collision *> m_Collisions;
        Cannon * m_Cannon;
        int m_Score;
        const geometry::Vector2D m_GameWorldDimensions;
        std::queue<GameCommand *> m_QueuedCommands;
        std::list<GameCommand *> m_ExecutedCommands;
        Memento * m_BaselineState;

        void updateGameObjects();
        void handleCollisions();
        void executeCommands();

        void addGameObject(Missile * o);
        void addGameObject(Enemy * o);
        void addGameObject(Collision * o);
        void addGameObject(Cannon * o);

        void destroyGameObject(GameObject * o);
        void removeAllGameObjects();
    };

}

#endif /* GAME_MODEL_H */
