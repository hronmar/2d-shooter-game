#include "method_call_command.h"
#include "abstract_game_model.h"

using namespace command;
using proxy::AbstractGameModel;

MethodCallCommand::MethodCallCommand(proxy::AbstractGameModel * receiver,
        void(proxy::AbstractGameModel:: * method)())
: GameCommand(receiver), m_Method(method) {
}

void MethodCallCommand::privateExecute() {
    (m_Receiver->*m_Method)();
}
