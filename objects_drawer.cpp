#include "objects_drawer.h"

#include <cmath>
#include <unordered_map>

#include "cannon.h"
#include "collision.h"
#include "missile.h"
#include "enemy.h"
#include "utility.h"

using namespace view;
using namespace flyweight;
using namespace geometry;

ObjectsDrawer::ObjectsDrawer(flyweight::ResourceLoader * resourceLoader)
: m_RenderTarget(nullptr), m_ResourceLoader(resourceLoader) {
}

void ObjectsDrawer::visit(model::Cannon & cannon) {
    if (m_RenderTarget == nullptr)
        return;

    sf::Sprite sprite(m_ResourceLoader->getTexture(CANNON_IMAGE)->get());
    int x = cannon.getPosition().getX(), y = cannon.getPosition().getY();
    int xOffset = sprite.getLocalBounds().width/2.0f, yOffset = sprite.getLocalBounds().height/2.0f;
    sprite.setPosition(x + xOffset, y + yOffset);
    sprite.setOrigin(sprite.getLocalBounds().left + xOffset,
               sprite.getLocalBounds().top  + yOffset);
    sprite.setRotation(Utility::radToDeg(cannon.getShootAngle()));
    m_RenderTarget->draw(sprite);
}

void ObjectsDrawer::visit(model::Collision & collision) {
    if (m_RenderTarget == nullptr)
        return;

    sf::Sprite sprite(m_ResourceLoader->getTexture(COLLISION_IMAGE)->get());
    sprite.setPosition(collision.getPosition().asSFMLVector());
    m_RenderTarget->draw(sprite);
}

void ObjectsDrawer::visit(model::Missile & missile) {
    if (m_RenderTarget == nullptr)
        return;

    sf::Sprite sprite(m_ResourceLoader->getTexture(MISSILE_IMAGE)->get());
    sprite.setPosition(missile.getPosition().asSFMLVector());
    m_RenderTarget->draw(sprite);
}

void ObjectsDrawer::visit(model::Enemy & enemy) {
    if (m_RenderTarget == nullptr)
        return;

    static std::unordered_map<model::Enemy *, int> assignedVariants;
    int variant;
    auto it = assignedVariants.find(&enemy);
    if (it != assignedVariants.end()) {
        variant = it->second;
    } else {
        variant = rand() % ENEMY_IMAGE_VARIANTS + 1;
        assignedVariants.insert(it, std::make_pair(&enemy, variant));
    }
    sf::Sprite sprite(m_ResourceLoader->getTexture(ENEMY_IMAGE_PREFIX + std::to_string(variant) + ".png")->get());
    sprite.setPosition(enemy.getPosition().asSFMLVector());
    m_RenderTarget->draw(sprite);
}

sf::RenderTarget * ObjectsDrawer::getRenderTarget() const {
    return m_RenderTarget;
}

void ObjectsDrawer::setRenderTarget(sf::RenderTarget * RenderTarget) {
    m_RenderTarget = RenderTarget;
}

void ObjectsDrawer::displayInfoText(const std::string & info) {
    sf::Text text;
    text.setFont(m_ResourceLoader->getFont(INFO_TEXT_FONT)->get());
    text.setString(info);
    text.setFillColor(sf::Color::Black);
    text.setCharacterSize(18);
    text.setPosition(35, 0);
    m_RenderTarget->draw(text);
}

void ObjectsDrawer::displayEndGameText(const std::string & message) {
    sf::Text text;
    text.setFont(m_ResourceLoader->getFont(INFO_TEXT_FONT)->get());
    text.setString(message);
    text.setStyle(sf::Text::Bold);
    text.setFillColor(sf::Color::Black);
    text.setOutlineColor(sf::Color::Red);
    text.setOutlineThickness(1.f);
    text.setCharacterSize(32);
    text.setPosition(50, 0.3f * m_RenderTarget->getSize().y);

    m_RenderTarget->draw(text);
}

const std::string ObjectsDrawer::CANNON_IMAGE = "resources/images/cannon.png";
const std::string ObjectsDrawer::COLLISION_IMAGE = "resources/images/collision.png";
const std::string ObjectsDrawer::ENEMY_IMAGE_PREFIX = "resources/images/enemy";
const int ObjectsDrawer::ENEMY_IMAGE_VARIANTS = 2;
const std::string ObjectsDrawer::MISSILE_IMAGE = "resources/images/missile.png";
const std::string ObjectsDrawer::INFO_TEXT_FONT = "resources/fonts/FifteenTwenty.otf";
