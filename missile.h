#ifndef MISSILE_H
#define MISSILE_H

#include "game_object.h"
#include "movement_strategy.h"

using geometry::Vector2D;
using strategy::MovementStrategy;

namespace model {

    class Missile : public GameObject {
    public:
        Missile(Vector2D velocity, MovementStrategy * movementStrategy);
        Missile(const Missile & other);
        virtual ~Missile();
        
        void move();

        Vector2D getSize() const override;

        void accept(visitor::Visitor & visitor) override;

        Missile * copy() const override;
    private:
        Vector2D m_Velocity;
        MovementStrategy * m_MovementStrategy;
    };

}

#endif /* MISSILE_H */
