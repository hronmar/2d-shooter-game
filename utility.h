#ifndef UTILITY_H
#define UTILITY_H

#include <cmath>
#include <climits>

namespace geometry {

    class Utility {
    public:

        static float degToRad(float deg) {
            return deg * M_PI * (1 / 180.0f);
        }

        static float radToDeg(float rad) {
            return rad * M_1_PI * 180.0f;
        }
        
        static bool cmpf(float x, float y) {
            return (fabsf(x - y) < std::numeric_limits<float>::epsilon());
        }
    };

}

#endif /* UTILITY_H */
