#ifndef VECTOR_2D_H
#define VECTOR_2D_H

#include <SFML/System/Vector2.hpp>

namespace geometry {
    
    class Rectangle;

    class Vector2D {
    public:
        Vector2D();
        Vector2D(float x, float y);
        Vector2D(sf::Vector2f vector);
        
        static const Vector2D ZERO;
        static const Vector2D UNIT_UP;
        static const Vector2D UNIT_DOWN;
        static const Vector2D UNIT_LEFT;
        static const Vector2D UNIT_RIGHT;

        float getX() const;
        void setX(float x);
        float getY() const;
        void setY(float y);
        sf::Vector2f asSFMLVector() const;

        void translateBy(Vector2D vector);
        Vector2D summedWith(Vector2D vector) const;

        void multiplyBy(float k);
        Vector2D multipliedBy(float k) const;
        Vector2D negated() const;

        float getEuclideanNorm() const;
        void normalize();
        Vector2D normalized() const;

        float distanceTo(Vector2D vector) const;
        bool equalTo(Vector2D vector) const;

        void rotateBy(float angle);
        Vector2D rotatedBy(float angle);

        Rectangle rectangleTo(Vector2D other) const;
        
    private:
        sf::Vector2f m_Vector;
    };

}

#endif /* VECTOR_2D_H */
