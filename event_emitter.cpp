#include "event_emitter.h"

#include "null_event_handler.h"

using namespace events;

EventEmitter::EventEmitter() : m_Handler(new NullEventHandler()) {
}

EventEmitter::~EventEmitter() {
    delete m_Handler;
}

EventHandler * EventEmitter::getHandler() const {
    return m_Handler;
}

void EventEmitter::setHandler(EventHandler * handler) {
    delete m_Handler;
    if (handler == nullptr)
        handler = new NullEventHandler();
    m_Handler = handler;
}

void EventEmitter::emitEvent(GameEvent event) {
    m_Handler->handleEvent(event);
}
