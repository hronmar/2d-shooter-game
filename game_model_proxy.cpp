#include "game_model_proxy.h"

#include <iostream>
#include <iomanip>
#include <ctime>
#include <chrono>

#include "config.h"

using namespace proxy;
using command::GameCommand;

GameModelProxy::GameModelProxy(model::GameModel * model) : m_Wrapee(model) {
}

GameModelProxy::~GameModelProxy() {
    delete m_Wrapee;
}

void GameModelProxy::aimCannonDown() {
    logEvent("called aimCannonDown()");
    m_Wrapee->aimCannonDown();
}

void GameModelProxy::aimCannonUp() {
    logEvent("called aimCannonUp()");
    m_Wrapee->aimCannonUp();
}

void GameModelProxy::decreaseForce() {
    logEvent("called decreaseForce()");
    m_Wrapee->decreaseForce();
}

const std::list<GameObject*>& GameModelProxy::getGameObjects() const {
    return m_Wrapee->getGameObjects();
}

int GameModelProxy::getScore() const {
    return m_Wrapee->getScore();
}

std::string GameModelProxy::getTextInfo() const {
    return m_Wrapee->getTextInfo();
}

model::Cannon * GameModelProxy::getCannon() const {
    return m_Wrapee->getCannon();
}

void GameModelProxy::increaseForce() {
    logEvent("called increaseForce()");
    m_Wrapee->increaseForce();
}

void GameModelProxy::moveCannonDown() {
    logEvent("called moveCannonDown()");
    m_Wrapee->moveCannonDown();
}

void GameModelProxy::moveCannonUp() {
    logEvent("called moveCannonUp()");
    m_Wrapee->moveCannonUp();
}

void GameModelProxy::shootCannon() {
    logEvent("called shootCannon()");
    if (m_Clock.getElapsedTime().asMilliseconds() > Config::get().getCannonCalldown()) {
        m_Wrapee->shootCannon();
        m_Clock.restart();
    } else
        logEvent("cannon did not shoot - too early after the previous call");
}

void GameModelProxy::tick() {
    m_Wrapee->tick();
}

void GameModelProxy::toggleShootingMode() {
    logEvent("called toggleShootingMode()");
    m_Wrapee->toggleShootingMode();
}

bool GameModelProxy::isGameEnd() const {
    return m_Wrapee->isGameEnd();
}

void GameModelProxy::reset() {
    logEvent("resetting model");
    m_Wrapee->reset();
}

void GameModelProxy::registerCommand(GameCommand * command) {
    m_Wrapee->registerCommand(command);
}

void GameModelProxy::undoLastCommand() {
    logEvent("undo last action");
    m_Wrapee->undoLastCommand();
}

Memento * GameModelProxy::createMemento() {
    return m_Wrapee->createMemento();
}

void GameModelProxy::setMemento(Memento * memento) {
    m_Wrapee->setMemento(memento);
}

void GameModelProxy::attachObserver(observer::Observer * observer) {
    m_Wrapee->attachObserver(observer);
}

void GameModelProxy::detachObserver(observer::Observer * observer) {
    m_Wrapee->detachObserver(observer);
}

void GameModelProxy::notifyObservers() {
    m_Wrapee->notifyObservers();
}

events::EventHandler * GameModelProxy::getHandler() const {
    return m_Wrapee->getHandler();
}

void GameModelProxy::setHandler(events::EventHandler * handler) {
    m_Wrapee->setHandler(handler);
}

void GameModelProxy::logEvent(std::string msg) const {
    auto t = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
    std::cout << std::put_time(std::localtime(&t), "%F %T")
            << ": " << "GameModelProxy: " << msg << std::endl;
}
