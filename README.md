# Semestrální práce MI-ADP

## Použité návrhové vzory

### Povinné
- MVC
- Proxy
- State
- Visitor
- Observer
- Memento
- Abstract
- Command
- Strategy

### Další použité vzory
- Flyweight (namespace flyweight) - pro práci s texturami, zvuky, atd.
- Chain of Responsibility (namespace events) - mechanismus
pro vysílání a zpracování událostí z modelu
- Composite (namespace events) - spolu s Chain of Responsibility
- Singleton (config.h) - konfigurace aplikace
- Null object pattern (null_event_handler.h) - NullEventHandler

