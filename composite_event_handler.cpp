#include <algorithm>

#include "composite_event_handler.h"

using namespace events;

CompositeEventHandler::~CompositeEventHandler() {
    for (auto child : m_Childs)
        delete child;
}

void CompositeEventHandler::handleEvent(GameEvent event) {
    for (auto child : m_Childs)
        child->handleEvent(event);

    if (m_Next)
        m_Next->handleEvent(event);
}

void CompositeEventHandler::addChild(EventHandler * handler) {
    if (std::find(m_Childs.begin(), m_Childs.end(), handler) == m_Childs.end())
        m_Childs.push_back(handler);
}

void CompositeEventHandler::removeChild(EventHandler * handler) {
    m_Childs.remove(handler);
}
