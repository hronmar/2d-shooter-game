#ifndef GAME_WINDOW_H
#define GAME_WINDOW_H

#include <SFML/Graphics.hpp>

#include "game_controller.h"
#include "abstract_game_model.h"
#include "objects_drawer.h"
#include "observer.h"

using proxy::AbstractGameModel;

namespace view {

    /* The view in MVC. This is the application window. */
    class GameWindow : public sf::RenderWindow, public observer::Observer {
    public:
        explicit GameWindow(controller::GameController * controller);

        AbstractGameModel * getModel() const;
        void setModel(AbstractGameModel * model);

        void processEvents();

        void update() override;

    private:
        controller::GameController * m_Controller;
        AbstractGameModel * m_Model;
        flyweight::ResourceLoader m_ResourceLoader;
        ObjectsDrawer m_Drawer;

        void render();
        void renderScene();
        void displayEndGameScreen();
    };

}

#endif /* GAME_WINDOW_H */
