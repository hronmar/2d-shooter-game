#include "single_shooting_mode.h"

#include "cannon.h"
#include "double_shooting_mode.h"

using namespace state;
using model::Cannon;

SingleShootingMode::SingleShootingMode(Cannon * context) : CannonShootingMode(context) {
}

void SingleShootingMode::shoot() {
    m_Context->addShot();
}

void SingleShootingMode::toggleMode() {
    m_Context->setShootingMode(new DoubleShootingMode(m_Context));
}

std::string SingleShootingMode::printString() const {
    return "Single shooting mode";
}

SingleShootingMode * SingleShootingMode::newInContext(model::Cannon * context) const {
    return new SingleShootingMode(context);
}
