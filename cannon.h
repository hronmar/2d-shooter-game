#ifndef CANNON_H
#define CANNON_H

#include <list>

#include "game_object.h"
#include "missile.h"
#include "cannon_shooting_mode.h"

using geometry::Vector2D;
using state::CannonShootingMode;

namespace abstract_factory {
    class GameObjectsFactory;
}

namespace model {

    class Cannon : public GameObject {
    public:
        Cannon(abstract_factory::GameObjectsFactory * factory);

        Cannon(const Cannon & other);

        virtual ~Cannon();

        void moveUp();
        void moveDown();
        void aimUp();
        void aimDown();
        void increaseForce();
        void decreaseForce();

        std::list<Missile *> shoot();

        float getShootAngle() const;
        float getShootForce() const;

        void toggleShootingMode();
        std::string getShootingModeInfo() const;
        void setShootingMode(CannonShootingMode * shootingMode);

        int getShotsFired() const;

        void addShot();

        Vector2D getSize() const override;

        void accept(visitor::Visitor & visitor) override;

        Cannon * copy() const override;
    private:
        float m_ShootAngle;
        float m_ShootForce;

        CannonShootingMode * m_ShootingMode;
        std::list<Missile *> m_AccumulatedShots;
        abstract_factory::GameObjectsFactory * m_Factory;

        int m_ShotsFired;
    };

}

#endif /* CANNON_H */
