#ifndef REALISTIC_GAME_OBJECTS_FACTORY_H
#define REALISTIC_GAME_OBJECTS_FACTORY_H

#include "game_objects_factory.h"

#include "realistic_enemy.h"

using namespace model;
using geometry::Vector2D;

namespace abstract_factory {

    class RealisticGameObjectsFactory : public GameObjectsFactory {
    public:

        Cannon * createCannon() override;
        Collision * createCollision(Vector2D position) override;
        RealisticEnemy * createEnemy(Vector2D position) override;
        Missile * createMissile(Vector2D position, Vector2D velocity) override;
    };

}

#endif /* REALISTIC_GAME_OBJECTS_FACTORY_H */
