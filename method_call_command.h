#ifndef METHOD_CALL_COMMAND_H
#define METHOD_CALL_COMMAND_H

#include "game_command.h"

namespace command {

    /* Command encapsulating a member function call */
    class MethodCallCommand : public GameCommand {
    public:
        MethodCallCommand(proxy::AbstractGameModel * receiver,
                void(proxy::AbstractGameModel:: * method)());

    protected:        
        void privateExecute() override;

    private:
        void(proxy::AbstractGameModel:: * m_Method)();
    };

}

#endif /* METHOD_CALL_COMMAND_H */
