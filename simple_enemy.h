#ifndef SIMPLE_ENEMY_H
#define SIMPLE_ENEMY_H

#include "enemy.h"

namespace model {

    /* Simple, stationary enemy */
    class SimpleEnemy : public Enemy {
    public:

        void move() override;

        geometry::Vector2D getSize() const override;

        SimpleEnemy * copy() const override;
    private:

    };

}

#endif /* SIMPLE_ENEMY_H */
