#ifndef COMPOSITE_EVENT_HANDLER_H
#define COMPOSITE_EVENT_HANDLER_H

#include "event_handler.h"

#include <list>

namespace events {

    class CompositeEventHandler : public EventHandler {
    public:

        virtual ~CompositeEventHandler();

        void addChild(EventHandler * handler);
        void removeChild(EventHandler * handler);

        void handleEvent(GameEvent event) override;

    private:
        std::list<EventHandler *> m_Childs;

    };

}

#endif /* COMPOSITE_EVENT_HANDLER_H */
