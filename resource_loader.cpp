#include "resource_loader.h"

using namespace flyweight;

ResourceLoader::ResourceLoader() {
}

ResourceLoader::~ResourceLoader() {
    for (auto tmp : m_Textures)
        delete tmp.second;
    for (auto tmp : m_Fonts)
        delete tmp.second;
    for (auto tmp : m_Sounds)
        delete tmp.second;
}

TextureFlyweight * ResourceLoader::getTexture(const std::string & path) {
    if (m_Textures.count(path) == 0) {
        m_Textures[path] = new TextureFlyweight(path);
        m_Textures[path]->get().setSmooth(true); //enable anti-aliasing
    }

    return m_Textures[path];
}

FontFlyweight * ResourceLoader::getFont(const std::string& path) {
    if (m_Fonts.count(path) == 0)
        m_Fonts[path] = new FontFlyweight(path);

    return m_Fonts[path];
}

SoundFlyweight * ResourceLoader::getSound(const std::string& path) {
    if (m_Sounds.count(path) == 0)
        m_Sounds[path] = new SoundFlyweight(path);

    return m_Sounds[path];
}
