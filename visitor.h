#ifndef VISITOR_H
#define VISITOR_H

//forward declarations
namespace model {
    class Cannon;
    class Collision;
    class Enemy;
    class Missile;
}

namespace visitor {

    class Visitor {
    public:

        virtual ~Visitor() {
        }

        virtual void visit(model::Cannon & cannon) = 0;
        virtual void visit(model::Collision & collision) = 0;
        virtual void visit(model::Enemy & enemy) = 0;
        virtual void visit(model::Missile & missile) = 0;

    };

}

#endif /* VISITOR_H */
