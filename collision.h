#ifndef COLLISION_H
#define COLLISION_H

#include "game_object.h"

namespace model {

    class Collision : public GameObject {
    public:
        Collision();

        bool isAlive();
        void tick();

        void accept(visitor::Visitor & visitor) override;
        
        Collision * copy() const override;
    private:
        int m_Lifespan;
    };

}

#endif /* COLLISION_H */
