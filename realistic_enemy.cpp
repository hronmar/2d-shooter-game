#include "realistic_enemy.h"

#include "visitor.h"

using namespace model;
using geometry::Vector2D;

RealisticEnemy::RealisticEnemy(Vector2D startingPos, bool movingUp) :
m_StartingPos(startingPos), m_MovingUp(movingUp), m_I(MAX_I / 2) {
    setPosition(startingPos);
}

const int RealisticEnemy::MAX_I = 50;

void RealisticEnemy::move() {
    if (m_MovingUp)
        m_Position.translateBy(Vector2D::UNIT_UP);
    else
        m_Position.translateBy(Vector2D::UNIT_DOWN);

    if (++m_I >= MAX_I) {
        m_MovingUp = !m_MovingUp;
        m_I = 0;
    }
}

Vector2D RealisticEnemy::getSize() const {
    return Vector2D(30, 30);
}

RealisticEnemy * RealisticEnemy::copy() const {
    return new RealisticEnemy(*this);
}
