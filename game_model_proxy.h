#ifndef GAME_MODEL_PROXY_H
#define GAME_MODEL_PROXY_H

#include <SFML/System/Clock.hpp>

#include "abstract_game_model.h"
#include "game_model.h"

namespace proxy {

    class GameModelProxy : public AbstractGameModel {
    public:
        GameModelProxy(model::GameModel * model);
        virtual ~GameModelProxy();

        void aimCannonDown() override;
        void aimCannonUp() override;
        void decreaseForce() override;
        const std::list<GameObject*> & getGameObjects() const override;
        int getScore() const override;
        std::string getTextInfo() const override;
        model::Cannon * getCannon() const override;
        void increaseForce() override;
        void moveCannonDown() override;
        void moveCannonUp() override;
        void shootCannon() override;
        void tick() override;
        void toggleShootingMode() override;

        bool isGameEnd() const override;
        void reset() override;

        void registerCommand(command::GameCommand * command) override;
        void undoLastCommand() override;

        Memento * createMemento() override;
        void setMemento(Memento * memento) override;

        void attachObserver(observer::Observer * observer) override;
        void detachObserver(observer::Observer * observer) override;
        void notifyObservers() override;

        events::EventHandler * getHandler() const override;
        void setHandler(events::EventHandler * handler) override;


    protected:
        model::GameModel * m_Wrapee;

        void logEvent(std::string msg) const;

    private:
        sf::Clock m_Clock;

    };

}

#endif /* GAME_MODEL_PROXY_H */
