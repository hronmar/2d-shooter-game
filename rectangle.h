#ifndef RECTANGLE_H
#define RECTANGLE_H

#include <SFML/Graphics/Rect.hpp>

#include "vector_2D.h"

namespace geometry {

    class Rectangle {
    public:
        Rectangle(Vector2D position, Vector2D size);

        bool contains(float x, float y) const;
        bool contains(Vector2D point) const;
        bool intersectsWith(Rectangle rect) const;
        bool isSubsetOf(Rectangle rect) const;

    private:
        sf::Rect<float> m_Rect;
    };

}

#endif /* RECTANGLE_H */
