#ifndef ENEMY_DIE_EVENT_HANDLER_H
#define ENEMY_DIE_EVENT_HANDLER_H

#include <SFML/Audio/Sound.hpp>

#include "event_handler.h"
#include "resource_loader.h"

namespace events {

    class EnemyDieEventHandler : public EventHandler {
    public:

        EnemyDieEventHandler(EventHandler * nextInChain, flyweight::ResourceLoader * resourceLoader);
        EnemyDieEventHandler(flyweight::ResourceLoader * resourceLoader);

        void handleEvent(GameEvent event) override;

    private:
        flyweight::ResourceLoader * m_ResourceLoader;
        
        sf::Sound m_Sound;
        static const std::string ENEMY_DIE_SOUND;
    };

}

#endif /* ENEMY_DIE_EVENT_HANDLER_H */
