#include "simple_game_objects_factory.h"
#include "simple_movement_strategy.h"

using namespace abstract_factory;
using namespace model;
using geometry::Vector2D;
using strategy::SimpleMovementStrategy;

Cannon * SimpleGameObjectsFactory::createCannon() {
    return new Cannon(this);
}

Collision * SimpleGameObjectsFactory::createCollision(Vector2D position) {
    Collision * c = new Collision();
    c->setPosition(position);
    return c;
}

SimpleEnemy * SimpleGameObjectsFactory::createEnemy(Vector2D position) {
    SimpleEnemy * e = new SimpleEnemy();
    e->setPosition(position);
    return e;
}

Missile * SimpleGameObjectsFactory::createMissile(Vector2D position, Vector2D velocity) {
    Missile * m = new Missile(velocity, new SimpleMovementStrategy());
    m->setPosition(position);
    return m;
}
