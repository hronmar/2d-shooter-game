#ifndef REALISTIC_MOVEMENT_STRATEGY_H
#define REALISTIC_MOVEMENT_STRATEGY_H

#include "movement_strategy.h"

using geometry::Vector2D;

namespace strategy {

    /* Movement strategy simulating a ballistic curve */
    class RealisticMovementStrategy : public MovementStrategy {
    public:

        Vector2D nextPosition(Vector2D currentPosition, Vector2D currentVelocity) override;
        Vector2D nextVelocity(Vector2D currentVelocity) override;
        
        RealisticMovementStrategy * copy() const override;
    };

}

#endif /* REALISTIC_MOVEMENT_STRATEGY_H */

