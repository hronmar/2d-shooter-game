#include "game_model.h"

#include <random>
#include <sstream>
#include <unordered_set>

#include "vector_2D.h"
#include "config.h"
#include "utility.h"

using namespace model;
using namespace geometry;
using command::GameCommand;
using namespace events;

Memento::~Memento() {
    for (auto * tmp : m_Missiles)
        delete tmp;
    for (auto * tmp : m_Enemies)
        delete tmp;
    for (auto * tmp : m_Collisions)
        delete tmp;
    delete m_Cannon;
}

GameModel::GameModel(GameObjectsFactory * factory) : m_Factory(factory), m_Score(0), m_Cannon(nullptr),
m_GameWorldDimensions(Config::get().getGameWidth(), Config::get().getGameHeight()) {
    addGameObject(m_Factory->createCannon());

    //add enemies
    const float margin = 80; //dont add on the edges
    std::random_device rd;
    std::mt19937 rng(rd());
    std::uniform_int_distribution<int> uni_x(margin, m_GameWorldDimensions.getX() - margin);
    std::uniform_int_distribution<int> uni_y(margin, m_GameWorldDimensions.getY() - margin);

    for (int i = 0; i < Config::get().getMaxEnemies(); i++) {
        Enemy * e = m_Factory->createEnemy(Vector2D::ZERO);
        bool conflict;
        int attempts = 0;
        do {
            //check for conflicts with already added enemies
            e->setPosition(Vector2D(uni_x(rng), uni_y(rng)));
            conflict = false;
            for (auto o : m_Enemies)
                if (e->collidesWith(o)) {
                    conflict = true;
                    break;
                }
        } while (conflict && ++attempts < 1000);
        addGameObject(e);
    }

    m_BaselineState = createMemento();
}

GameModel::~GameModel() {
    removeAllGameObjects();

    if (m_Factory)
        delete m_Factory;

    for (GameCommand * cmd : m_ExecutedCommands)
        if (cmd->needsDestruction())
            delete cmd;

    if (m_BaselineState)
        delete m_BaselineState;
}

void GameModel::tick() {
    executeCommands();
    updateGameObjects();
    handleCollisions();

    notifyObservers();
}

void GameModel::updateGameObjects() {
    //move missiles
    for (auto it = m_Missiles.begin(); it != m_Missiles.end();) {
        (*it)->move();
        if ((*it)->getPosition().getY() > m_GameWorldDimensions.getX()) {
            destroyGameObject(static_cast<GameObject *> (*it));
            it = m_Missiles.erase(it);
        } else
            ++it;
    }

    //move enemies
    for (auto e : m_Enemies)
        e->move();

    //remove old collisions
    for (auto it = m_Collisions.begin(); it != m_Collisions.end();) {
        (*it)->tick();
        if (!(*it)->isAlive()) {
            destroyGameObject(static_cast<GameObject *> (*it));
            it = m_Collisions.erase(it);
        } else
            ++it;
    }
}

void GameModel::handleCollisions() {
    for (auto mIt = m_Missiles.begin(); mIt != m_Missiles.end();) {
        bool erased = false;
        for (auto eIt = m_Enemies.begin(); eIt != m_Enemies.end(); ++eIt) {
            if ((*mIt)->collidesWith(*eIt)) {
                m_Score++;
                addGameObject(m_Factory->createCollision((*eIt)->getPosition()));

                emitEvent(GameEvent::COLLISION);

                //remove missile and enemy
                destroyGameObject(static_cast<GameObject *> (*eIt));
                m_Enemies.erase(eIt);
                destroyGameObject(static_cast<GameObject *> (*mIt));
                mIt = m_Missiles.erase(mIt);
                erased = true;
                break;
            }
        }

        if (!erased)
            ++mIt;
    }
}

void GameModel::executeCommands() {
    while (!m_QueuedCommands.empty()) {
        GameCommand * cmd = m_QueuedCommands.front();
        m_QueuedCommands.pop();
        cmd->execute();

        m_ExecutedCommands.push_back(cmd);
    }
}

const std::list<GameObject*>& GameModel::getGameObjects() const {
    return m_GameObjects;
}

int GameModel::getScore() const {
    return m_Score;
}

std::string GameModel::getTextInfo() const {
    std::stringstream s;
    s << "Score: " << m_Score
            << " | Angle: " << -static_cast<int> (geometry::Utility::radToDeg(m_Cannon->getShootAngle()))
            << " | Force: " << static_cast<int> (m_Cannon->getShootForce())
            << " | " << m_Cannon->getShootingModeInfo();
    return s.str();
}

model::Cannon * GameModel::getCannon() const {
    return m_Cannon;
}

void GameModel::moveCannonUp() {
    m_Cannon->moveUp();
    notifyObservers();
}

void GameModel::moveCannonDown() {
    m_Cannon->moveDown();
    notifyObservers();
}

void GameModel::aimCannonUp() {
    m_Cannon->aimUp();
    notifyObservers();
}

void GameModel::aimCannonDown() {
    m_Cannon->aimDown();
    notifyObservers();
}

void GameModel::increaseForce() {
    m_Cannon->increaseForce();
    notifyObservers();
}

void GameModel::decreaseForce() {
    m_Cannon->decreaseForce();
    notifyObservers();
}

void GameModel::shootCannon() {
    auto missilesShot = m_Cannon->shoot();
    for (Missile * m : missilesShot)
        addGameObject(m);
    emitEvent(GameEvent::CANNON_SHOOT);
}

void GameModel::toggleShootingMode() {
    m_Cannon->toggleShootingMode();
    notifyObservers();
}

bool GameModel::isGameEnd() const {
    return (m_Enemies.size() == 0);
}

void GameModel::reset() {
    setMemento(m_BaselineState);
    m_BaselineState = createMemento();
}

void GameModel::registerCommand(GameCommand * command) {
    m_QueuedCommands.push(command);
}

void GameModel::undoLastCommand() {
    if (m_ExecutedCommands.empty())
        return;

    m_ExecutedCommands.back()->undo();
    if (m_ExecutedCommands.back()->needsDestruction())
        delete m_ExecutedCommands.back();
    m_ExecutedCommands.pop_back();
}

Memento * GameModel::createMemento() {
    Memento * memento = new Memento();

    memento->m_Score = m_Score;
    for (auto m : m_Missiles)
        memento->m_Missiles.push_back(m->copy());
    for (auto e : m_Enemies)
        memento->m_Enemies.push_back(e->copy());
    for (auto c : m_Collisions)
        memento->m_Collisions.push_back(c->copy());
    memento->m_Cannon = m_Cannon->copy();

    return memento;
}

void GameModel::setMemento(Memento * memento) {
    removeAllGameObjects();

    m_Score = memento->m_Score;
    for (auto m : memento->m_Missiles)
        addGameObject(m);
    for (auto e : memento->m_Enemies)
        addGameObject(e);
    for (auto c : memento->m_Collisions)
        addGameObject(c);
    addGameObject(memento->m_Cannon);

    //we took ownership of all the objects
    memento->m_Missiles.clear();
    memento->m_Enemies.clear();
    memento->m_Collisions.clear();
    memento->m_Cannon = nullptr;
    //destroy the memento
    delete memento;
}

void GameModel::addGameObject(Missile * o) {
    m_Missiles.push_back(o);
    m_GameObjects.push_back(o);
}

void GameModel::addGameObject(Enemy * o) {
    m_Enemies.push_back(o);
    m_GameObjects.push_back(o);
}

void GameModel::addGameObject(Collision * o) {
    m_Collisions.push_back(o);
    m_GameObjects.push_back(o);
}

void GameModel::addGameObject(Cannon * o) {
    if (m_Cannon != nullptr) {
        //only one cannon allowed
        destroyGameObject(static_cast<GameObject *> (m_Cannon));
        m_Cannon = nullptr;
    }
    m_Cannon = o;
    m_GameObjects.push_back(o);
}

void GameModel::destroyGameObject(GameObject * o) {
    m_GameObjects.remove(o);
    delete o;
}

void GameModel::removeAllGameObjects() {
    auto objects = getGameObjects();
    for (auto o : objects)
        destroyGameObject(o);

    m_Missiles.clear();
    m_Enemies.clear();
    m_Collisions.clear();
    m_Cannon = nullptr;
}
