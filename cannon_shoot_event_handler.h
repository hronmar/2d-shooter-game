#ifndef CANNON_SHOOT_EVENT_HANDLER_H
#define CANNON_SHOOT_EVENT_HANDLER_H

#include <SFML/Audio/Sound.hpp>

#include "event_handler.h"
#include "resource_loader.h"

namespace events {

    class CannonShootEventHandler : public EventHandler {
    public:

        CannonShootEventHandler(EventHandler * nextInChain, flyweight::ResourceLoader * resourceHolder);
        CannonShootEventHandler(flyweight::ResourceLoader * resourceLoader);

        void handleEvent(GameEvent event) override;

    private:
        flyweight::ResourceLoader * m_ResourceLoader;

        sf::Sound m_Sound;
        static const std::string SHOT_SOUND;
    };

}

#endif /* CANNON_SHOOT_EVENT_HANDLER_H */
