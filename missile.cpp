#include "missile.h"

#include "visitor.h"

using namespace model;
using geometry::Vector2D;

Missile::Missile(Vector2D velocity, MovementStrategy * movementStrategy) :
m_Velocity(velocity), m_MovementStrategy(movementStrategy) {
}

Missile::Missile(const Missile& other) : GameObject(other), 
m_Velocity(other.m_Velocity), m_MovementStrategy(other.m_MovementStrategy->copy()) {
}

void Missile::move() {
    setPosition(m_MovementStrategy->nextPosition(getPosition(), m_Velocity));
    m_Velocity = m_MovementStrategy->nextVelocity(m_Velocity);
}


Missile::~Missile() {
    if (m_MovementStrategy)
        delete m_MovementStrategy;
}

Vector2D Missile::getSize() const {
    return Vector2D(14, 14);
}

void Missile::accept(visitor::Visitor & visitor) {
    visitor.visit(*this);
}

Missile * Missile::copy() const {
    return new Missile(*this);
}
