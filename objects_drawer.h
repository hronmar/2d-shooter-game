#ifndef OBJECTS_DRAWER_H
#define OBJECTS_DRAWER_H

#include <SFML/Graphics.hpp>

#include "visitor.h"
#include "resource_loader.h"

namespace view {

    /* Class for drawing graphical entities */
    class ObjectsDrawer : public visitor::Visitor {
    public:
        ObjectsDrawer(flyweight::ResourceLoader * resourceLoader);

        void visit(model::Cannon & cannon) override;
        void visit(model::Collision & collision) override;
        void visit(model::Missile & missile) override;
        void visit(model::Enemy & enemy) override;

        sf::RenderTarget * getRenderTarget() const;
        void setRenderTarget(sf::RenderTarget * RenderTarget);

        void displayInfoText(const std::string & info);

        void displayEndGameText(const std::string & message);

    private:
        sf::RenderTarget * m_RenderTarget;

        flyweight::ResourceLoader * m_ResourceLoader;

        static const std::string CANNON_IMAGE;
        static const std::string COLLISION_IMAGE;
        static const std::string ENEMY_IMAGE_PREFIX;
        static const int ENEMY_IMAGE_VARIANTS;
        static const std::string MISSILE_IMAGE;
        static const std::string INFO_TEXT_FONT;

    };
}

#endif /* OBJECTS_DRAWER_H */
