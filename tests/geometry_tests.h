#ifndef GEOMETRY_TESTS_H
#define GEOMETRY_TESTS_H

#include <cppunit/extensions/HelperMacros.h>

class GeometryTests : public CPPUNIT_NS::TestFixture {
    CPPUNIT_TEST_SUITE(GeometryTests);

    CPPUNIT_TEST(testVector2DCreation);
    CPPUNIT_TEST(testVector2DAccessors);
    CPPUNIT_TEST(testVector2DOperations);
    CPPUNIT_TEST(testRectangle);
    CPPUNIT_TEST(testUtility);

    CPPUNIT_TEST_SUITE_END();

public:
    GeometryTests();
    virtual ~GeometryTests();
    void setUp();
    void tearDown();

protected:
    void testVector2DCreation();
    void testVector2DAccessors();
    void testVector2DOperations();
    void testRectangle();
    void testUtility();
};

#endif /* GEOMETRY_TESTS_H */
