#ifndef GAME_MODEL_TESTS_H
#define GAME_MODEL_TESTS_H

#include <cppunit/extensions/HelperMacros.h>

#include "../game_model.h"

class GameModelTests : public CPPUNIT_NS::TestFixture {
    CPPUNIT_TEST_SUITE(GameModelTests);

    CPPUNIT_TEST(testCannonMovement);
    CPPUNIT_TEST(testSettingsCannonAim);
    CPPUNIT_TEST(testSettingsShootForce);
    CPPUNIT_TEST(testToggleShootingMode);
    CPPUNIT_TEST(testCommandsProcessing);

    CPPUNIT_TEST_SUITE_END();

public:
    GameModelTests();
    virtual ~GameModelTests();
    void setUp() override;
    void tearDown() override;

protected:

    void testCannonMovement();
    void testSettingsCannonAim();
    void testSettingsShootForce();
    void testToggleShootingMode();
    void testCommandsProcessing();

private:
    model::GameModel * m_TestObj;
};

#endif /* GAME_MODEL_TESTS_H */
