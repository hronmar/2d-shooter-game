#include "observer_tests.h"

#include <fakeit.hpp>

#include "../observer.h"
#include "../subject.h"

using namespace fakeit;
using namespace observer;

CPPUNIT_TEST_SUITE_REGISTRATION(ObserverTests);

ObserverTests::ObserverTests() {
}

ObserverTests::~ObserverTests() {
}

void ObserverTests::setUp() {
}

void ObserverTests::tearDown() {
}

void ObserverTests::testObserverNotification() {
    Mock<Observer> observerMock;
    Subject subject;

    Fake(Method(observerMock, update));

    subject.attachObserver(&observerMock.get());
    subject.notifyObservers();

    try {
        Verify(Method(observerMock, update)).Exactly(Once);
    } catch (std::exception e) {
        //verification failed
        CPPUNIT_ASSERT(false);
    }
}

void ObserverTests::testObserverDetachment() {
    Mock<Observer> observerMock;
    Subject subject;

    Fake(Method(observerMock, update));

    subject.attachObserver(&observerMock.get());
    subject.detachObserver(&observerMock.get());
    subject.notifyObservers();

    try {
        Verify(Method(observerMock, update)).Never();
    } catch (std::exception e) {
        //verification failed
        CPPUNIT_ASSERT(false);
    }

    subject.attachObserver(&observerMock.get());
    subject.notifyObservers(); //this will trigger update
    try {
        Verify(Method(observerMock, update)).Exactly(Once);
    } catch (std::exception e) {
        //verification failed
        CPPUNIT_ASSERT(false);
    }

    subject.detachObserver(&observerMock.get());
    subject.notifyObservers(); //this should not trigger update
    try {
        Verify(Method(observerMock, update)).Exactly(Once);
    } catch (std::exception e) {
        //verification failed
        CPPUNIT_ASSERT(false);
    }
}
