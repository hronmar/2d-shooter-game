#ifndef OBSERVER_TESTS_H
#define OBSERVER_TESTS_H

#include <cppunit/extensions/HelperMacros.h>

class ObserverTests : public CPPUNIT_NS::TestFixture {
    CPPUNIT_TEST_SUITE(ObserverTests);

    CPPUNIT_TEST(testObserverNotification);
    CPPUNIT_TEST(testObserverDetachment);

    CPPUNIT_TEST_SUITE_END();

public:
    ObserverTests();
    virtual ~ObserverTests();
    void setUp();
    void tearDown();

protected:

    void testObserverNotification();
    void testObserverDetachment();

};

#endif /* OBSERVER_TESTS_H */
