#include "geometry_tests.h"

#include "../vector_2D.h"
#include "../rectangle.h"
#include "../utility.h"

using namespace geometry;

CPPUNIT_TEST_SUITE_REGISTRATION(GeometryTests);

GeometryTests::GeometryTests() {
}

GeometryTests::~GeometryTests() {
}

void GeometryTests::setUp() {
}

void GeometryTests::tearDown() {
}

void GeometryTests::testVector2DCreation() {
    double x = 4.0f, y = 5.0f;
    Vector2D vect(x, y);
    CPPUNIT_ASSERT(Utility::cmpf(vect.getX(), x) && Utility::cmpf(vect.getY(), y));

    Vector2D vect2(vect);
    CPPUNIT_ASSERT(Utility::cmpf(vect2.getX(), x) && Utility::cmpf(vect2.getY(), y));
}

void GeometryTests::testVector2DAccessors() {
    double x = 4.2f, y = 8.0f;
    Vector2D vect;
    vect.setX(0.f);
    vect.setX(x);
    vect.setY(0.f);
    vect.setY(y);
    CPPUNIT_ASSERT(Utility::cmpf(vect.getX(), x) && Utility::cmpf(vect.getY(), y));
}

void GeometryTests::testVector2DOperations() {
    Vector2D vectA(1.0f, 7.0f);
    Vector2D vectB(4.0f, 3.0f);

    CPPUNIT_ASSERT(Utility::cmpf(vectA.distanceTo(vectB), 5.0f));
    CPPUNIT_ASSERT(Utility::cmpf(vectB.getEuclideanNorm(), 5.0f));

    Vector2D vectANorm = vectA.normalized();
    CPPUNIT_ASSERT(Utility::cmpf(vectANorm.getEuclideanNorm(), 1.0f));
    double sizeA = vectA.getEuclideanNorm();
    vectA.multiplyBy(1.f / sizeA);
    CPPUNIT_ASSERT(vectA.equalTo(vectANorm));
    vectA.multiplyBy(sizeA);

    CPPUNIT_ASSERT(vectA.negated().equalTo(Vector2D(-1.0f, -7.0f)));
    CPPUNIT_ASSERT(vectA.summedWith(vectB).equalTo(Vector2D(5.0f, 10.0f)));
    CPPUNIT_ASSERT(vectA.multipliedBy(0.f).equalTo(Vector2D::ZERO));
}

void GeometryTests::testRectangle() {
    Rectangle rectA(Vector2D(0.f, 0.f), Vector2D(1.0f, 1.0f));
    Rectangle rectB(Vector2D(0.5f, 0.5f), Vector2D(0.25f, 0.4f));
    Rectangle rectC(Vector2D(0.5f, 2.0f), Vector2D(10.0f, 0.5f));
    Vector2D(0.5f, 2.0f).rectangleTo(Vector2D(4.5f, 2.5f));

    CPPUNIT_ASSERT(rectA.intersectsWith(rectB));
    CPPUNIT_ASSERT(rectB.isSubsetOf(rectA));
    CPPUNIT_ASSERT(!rectA.intersectsWith(rectC));
    CPPUNIT_ASSERT(!rectC.isSubsetOf(rectA));
    CPPUNIT_ASSERT(!rectA.isSubsetOf(rectC));
    CPPUNIT_ASSERT(rectA.contains(Vector2D(0.5f, 0.5f)));
    CPPUNIT_ASSERT(!rectB.contains(Vector2D(0.45f, 0.45f)));
}

void GeometryTests::testUtility() {
    CPPUNIT_ASSERT(Utility::cmpf(Utility::degToRad(180), M_PI));
    CPPUNIT_ASSERT(Utility::cmpf(Utility::radToDeg(M_PI_2), 90));
}
