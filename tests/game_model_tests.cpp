#include "game_model_tests.h"

#include <climits>
#include <cmath>
#include <fakeit.hpp>

#include "../config.h"
#include "../game_command.h"
#include "../simple_game_objects_factory.h"
#include "../utility.h"
#include "../vector_2D.h"
#include "../single_shooting_mode.h"
#include "../double_shooting_mode.h"

using namespace fakeit;

using namespace geometry;
using command::GameCommand;
using model::GameModel;
using abstract_factory::SimpleGameObjectsFactory;
using namespace state;

CPPUNIT_TEST_SUITE_REGISTRATION(GameModelTests);

GameModelTests::GameModelTests() {
}

GameModelTests::~GameModelTests() {
}

void GameModelTests::setUp() {
    m_TestObj = new model::GameModel(new abstract_factory::SimpleGameObjectsFactory());
}

void GameModelTests::tearDown() {
    delete m_TestObj;
}

void GameModelTests::testCannonMovement() {
    Vector2D origPos = m_TestObj->getCannon()->getPosition();
    m_TestObj->moveCannonDown();
    m_TestObj->moveCannonUp();
    Vector2D newPos = m_TestObj->getCannon()->getPosition();
    CPPUNIT_ASSERT(newPos.equalTo(origPos));

    Vector2D expectedPos = m_TestObj->getCannon()->getPosition();
    m_TestObj->moveCannonDown();
    m_TestObj->moveCannonUp();
    m_TestObj->moveCannonDown();
    m_TestObj->moveCannonDown();
    m_TestObj->moveCannonDown();
    newPos = m_TestObj->getCannon()->getPosition();
    expectedPos.translateBy(Vector2D::UNIT_DOWN.multipliedBy(3.0f * Config::get().getMoveStep()));
    CPPUNIT_ASSERT(newPos.equalTo(expectedPos));
}

void GameModelTests::testSettingsCannonAim() {
    float origAngle = m_TestObj->getCannon()->getShootAngle();
    m_TestObj->aimCannonUp();
    m_TestObj->aimCannonUp();
    m_TestObj->aimCannonDown();
    m_TestObj->aimCannonDown();
    float newAngle = m_TestObj->getCannon()->getShootAngle();
    CPPUNIT_ASSERT(Utility::cmpf(origAngle, newAngle));

    origAngle = m_TestObj->getCannon()->getShootAngle();
    m_TestObj->aimCannonDown();
    m_TestObj->aimCannonDown();
    m_TestObj->aimCannonUp();
    newAngle = m_TestObj->getCannon()->getShootAngle();
    float expectedAngle = origAngle + Config::get().getRotateStep();
    CPPUNIT_ASSERT(Utility::cmpf(newAngle, expectedAngle));
}

void GameModelTests::testSettingsShootForce() {
    float origForce = m_TestObj->getCannon()->getShootForce();
    m_TestObj->increaseForce();
    m_TestObj->decreaseForce();
    float newForce = m_TestObj->getCannon()->getShootForce();
    CPPUNIT_ASSERT(Utility::cmpf(origForce, newForce));

    origForce = m_TestObj->getCannon()->getShootForce();
    m_TestObj->increaseForce();
    m_TestObj->increaseForce();
    m_TestObj->increaseForce();
    m_TestObj->decreaseForce();
    newForce = m_TestObj->getCannon()->getShootForce();
    float expectedForce = origForce + 2.0f * Config::get().getCannonForceStep();
    CPPUNIT_ASSERT(Utility::cmpf(newForce, expectedForce));

    int maxIncrements = Config::get().getMaxCannonForce() / Config::get().getCannonForceStep() + 1;
    for (int i = 0; i < maxIncrements; i++)
        m_TestObj->increaseForce();
    newForce = m_TestObj->getCannon()->getShootForce();
    expectedForce = Config::get().getMaxCannonForce();
    CPPUNIT_ASSERT(Utility::cmpf(newForce, expectedForce));
}

void GameModelTests::testToggleShootingMode() {
    std::string mode = m_TestObj->getCannon()->getShootingModeInfo();
    state::CannonShootingMode * tmp = new SingleShootingMode(nullptr);
    std::string singleMode = tmp->printString();
    delete tmp;
    tmp = new DoubleShootingMode(nullptr);
    std::string doubleMode = tmp->printString();
    delete tmp;
    tmp = nullptr;

    CPPUNIT_ASSERT(mode == singleMode);

    m_TestObj->toggleShootingMode();
    mode = m_TestObj->getCannon()->getShootingModeInfo();

    CPPUNIT_ASSERT(mode == doubleMode);
}

void GameModelTests::testCommandsProcessing() {
    Mock<GameCommand> mock;
    GameModel model(new SimpleGameObjectsFactory());

    Fake(Method(mock, execute));
    Fake(Method(mock, undo));
    When(Method(mock, needsDestruction)).AlwaysReturn(false);

    GameCommand * gc = &mock.get();

    model.registerCommand(gc);
    model.tick();

    try {
        Verify(Method(mock, execute)).Exactly(Once);
    } catch (std::exception e) {
        //verification failed
        CPPUNIT_ASSERT(false);
    }

    model.undoLastCommand();

    try {
        Verify(Method(mock, undo)).Exactly(Once);
    } catch (std::exception e) {
        //verification failed
        CPPUNIT_ASSERT(false);
    }

}
