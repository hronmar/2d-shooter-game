#ifndef RESOURCE_LOADER_H
#define RESOURCE_LOADER_H

#include <unordered_map>

#include "resource_flyweight.h"

namespace flyweight {

    /* Class responsible for loading and caching resources. This is the flyweight factory. */
    class ResourceLoader {
    public:
        ResourceLoader();
        ~ResourceLoader();

        TextureFlyweight * getTexture(const std::string & path);
        FontFlyweight * getFont(const std::string & path);
        SoundFlyweight * getSound(const std::string & path);

    private:
        std::unordered_map<std::string, TextureFlyweight *> m_Textures;
        std::unordered_map<std::string, FontFlyweight *> m_Fonts;
        std::unordered_map<std::string, SoundFlyweight *> m_Sounds;

    };

}

#endif /* RESOURCE_LOADER_H */
