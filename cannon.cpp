#include "cannon.h"

#include "vector_2D.h"
#include "visitor.h"
#include "config.h"
#include "single_shooting_mode.h"
#include "game_objects_factory.h"

using namespace model;
using geometry::Vector2D;
using state::SingleShootingMode;
using abstract_factory::GameObjectsFactory;

Cannon::Cannon(GameObjectsFactory * factory) : m_ShootAngle(0),
m_ShootForce(Config::get().getMoveStep()),
m_ShootingMode(new SingleShootingMode(this)),
m_Factory(factory),
m_ShotsFired(0) {
}

Cannon::Cannon(const Cannon & other) : GameObject(other),
m_ShootAngle(other.m_ShootAngle), m_ShootForce(other.m_ShootForce),
m_ShootingMode(other.m_ShootingMode->newInContext(this)),
m_AccumulatedShots(), m_Factory(other.m_Factory),
m_ShotsFired(other.m_ShotsFired) {
}

Cannon::~Cannon() {
    delete m_ShootingMode;
}

void Cannon::moveUp() {
    m_Position.translateBy(Vector2D::UNIT_UP.multipliedBy(Config::get().getMoveStep()));
}

void Cannon::moveDown() {
    m_Position.translateBy(Vector2D::UNIT_DOWN.multipliedBy(Config::get().getMoveStep()));
}

void Cannon::aimUp() {
    m_ShootAngle -= Config::get().getRotateStep();
    float max = Config::get().getMaxCannonRotation();
    if (m_ShootAngle < -max)
        m_ShootAngle = -max;
}

void Cannon::aimDown() {
    m_ShootAngle += Config::get().getRotateStep();
    float max = Config::get().getMaxCannonRotation();
    if (m_ShootAngle > max)
        m_ShootAngle = max;
}

void Cannon::increaseForce() {
    m_ShootForce += Config::get().getCannonForceStep();
    if (m_ShootForce > Config::get().getMaxCannonForce())
        m_ShootForce = Config::get().getMaxCannonForce();
}

void Cannon::decreaseForce() {
    m_ShootForce -= Config::get().getCannonForceStep();
    if (m_ShootForce < Config::get().getCannonForceStep())
        m_ShootForce = Config::get().getCannonForceStep();
}

std::list<Missile *> Cannon::shoot() {
    m_ShootingMode->shoot();
    std::list<Missile *> missilesShot = m_AccumulatedShots;
    m_ShotsFired += m_AccumulatedShots.size();
    m_AccumulatedShots.clear();
    return missilesShot;
}

float Cannon::getShootAngle() const {
    return m_ShootAngle;
}

float Cannon::getShootForce() const {
    return m_ShootForce;
}

void Cannon::toggleShootingMode() {
    m_ShootingMode->toggleMode();
}

std::string Cannon::getShootingModeInfo() const {
    return m_ShootingMode->printString();
}

void Cannon::setShootingMode(CannonShootingMode* shootingMode) {
    if (m_ShootingMode)
        delete m_ShootingMode;
    m_ShootingMode = shootingMode;
}

int Cannon::getShotsFired() const {
    return m_ShotsFired;
}

void Cannon::addShot() {
    Vector2D velocityVect = Vector2D::UNIT_RIGHT.multipliedBy(m_ShootForce);
    velocityVect.rotateBy(m_ShootAngle);
    m_AccumulatedShots.push_back(m_Factory->createMissile(
            m_Position.summedWith(velocityVect.normalized().multipliedBy(getSize().getX() / 2.f))
                .summedWith(Vector2D::UNIT_DOWN.multipliedBy(getSize().getY()/4.f)), velocityVect));
}

Vector2D Cannon::getSize() const {
    return Vector2D(40, 40);
}

void Cannon::accept(visitor::Visitor & visitor) {
    visitor.visit(*this);
}

Cannon * Cannon::copy() const {
    return new Cannon(*this);
}
