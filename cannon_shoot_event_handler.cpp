#include "cannon_shoot_event_handler.h"

using namespace events;

CannonShootEventHandler::CannonShootEventHandler(EventHandler * nextInChain,
        flyweight::ResourceLoader * resourceHolder) :
EventHandler(nextInChain), m_ResourceLoader(resourceHolder) {
}

CannonShootEventHandler::CannonShootEventHandler(flyweight::ResourceLoader * resourceLoader) :
EventHandler(), m_ResourceLoader(resourceLoader) {
}

void CannonShootEventHandler::handleEvent(GameEvent event) {
    if (event == GameEvent::CANNON_SHOOT) {
        m_Sound.setBuffer(m_ResourceLoader->getSound(SHOT_SOUND)->get());
        m_Sound.play();
    } else if (m_Next)
        m_Next->handleEvent(event);
}

const std::string CannonShootEventHandler::SHOT_SOUND = "resources/sounds/shoot.wav";
