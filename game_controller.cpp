#include "game_controller.h"
#include "method_call_command.h"

using namespace controller;
using proxy::AbstractGameModel;
using command::MethodCallCommand;

GameController::GameController(AbstractGameModel * model) : m_Model(model) {
}

void GameController::processEvent(sf::Event event) {
    if (event.type == sf::Event::KeyPressed) {

        if (m_Model->isGameEnd()) {
            //reset model on any key if game over
            m_Model->reset();
            return;
        }

        switch (event.key.code) {
            case sf::Keyboard::Up:
            case sf::Keyboard::W:
                m_Model->registerCommand(new MethodCallCommand(m_Model,
                        &AbstractGameModel::moveCannonUp));
                break;

            case sf::Keyboard::Down:
            case sf::Keyboard::S:
                m_Model->registerCommand(new MethodCallCommand(m_Model,
                        &AbstractGameModel::moveCannonDown));
                break;

            case sf::Keyboard::Left:
            case sf::Keyboard::A:
                if (event.key.control)
                    m_Model->registerCommand(new MethodCallCommand(m_Model,
                        &AbstractGameModel::aimCannonUp));
                else
                    m_Model->registerCommand(new MethodCallCommand(m_Model,
                        &AbstractGameModel::decreaseForce));
                break;

            case sf::Keyboard::Right:
            case sf::Keyboard::D:
                if (event.key.control)
                    m_Model->registerCommand(new MethodCallCommand(m_Model,
                        &AbstractGameModel::aimCannonDown));
                else
                    m_Model->registerCommand(new MethodCallCommand(m_Model,
                        &AbstractGameModel::increaseForce));
                break;

            case sf::Keyboard::Q:
                m_Model->registerCommand(new MethodCallCommand(m_Model,
                        &AbstractGameModel::toggleShootingMode));
                break;

            case sf::Keyboard::Space:
            case sf::Keyboard::F:
                m_Model->registerCommand(new MethodCallCommand(m_Model,
                        &AbstractGameModel::shootCannon));
                break;

            case sf::Keyboard::Z:
                if (event.key.control) {
                    m_Model->undoLastCommand();
                }
                break;

            default:
                //do nothing
                break;
        }
    }

}

AbstractGameModel * GameController::getModel() const {
    return m_Model;
}
