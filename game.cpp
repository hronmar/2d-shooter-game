#include "game.h"

#include <SFML/Window.hpp>

#include "config.h"

#include "simple_game_objects_factory.h"
#include "realistic_game_objects_factory.h"

using namespace abstract_factory;
using model::GameModel;

Game::Game() : m_Model(new GameModel(Config::get().isRealisticMode()
                                    ? static_cast<GameObjectsFactory *> (new RealisticGameObjectsFactory())
                                    : static_cast<GameObjectsFactory *> (new SimpleGameObjectsFactory()))),
m_Controller(&m_Model), m_View(&m_Controller) {
}

void Game::run() {
    const float tick = Config::get().getSecondsPerFrame();
    sf::Clock clock;
    float elapsedTime = 0;

    while (m_View.isOpen()) {
        elapsedTime += clock.restart().asSeconds();

        m_View.processEvents();

        while (elapsedTime >= tick) {
            elapsedTime -= tick;
            m_Model.tick();
        }
    }
}
