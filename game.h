#ifndef GAME_H
#define GAME_H

#include "game_model_proxy.h"
#include "game_window.h"
#include "game_controller.h"

class Game {
public:
    Game();

    void run();

private:
    proxy::GameModelProxy m_Model;
    controller::GameController m_Controller;
    view::GameWindow m_View;
};

#endif /* GAME_H */
