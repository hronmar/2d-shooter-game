#include "collision.h"

#include "visitor.h"
#include "config.h"

using namespace model;

Collision::Collision() : m_Lifespan(Config::get().getCollisionLifespan()) {
}

bool Collision::isAlive() {
    return m_Lifespan > 0;
}

void Collision::tick() {
    --m_Lifespan;
}

void Collision::accept(visitor::Visitor & visitor) {
    visitor.visit(*this);
}

Collision* Collision::copy() const {
    return new Collision(*this);
}
