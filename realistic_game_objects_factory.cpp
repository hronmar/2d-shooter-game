#include "realistic_game_objects_factory.h"

#include "realistic_movement_strategy.h"

using namespace abstract_factory;
using namespace model;
using geometry::Vector2D;
using strategy::RealisticMovementStrategy;

Cannon * RealisticGameObjectsFactory::createCannon() {
    return new Cannon(this);
}

Collision * RealisticGameObjectsFactory::createCollision(Vector2D position) {
    Collision * c = new Collision();
    c->setPosition(position);
    return c;
}

RealisticEnemy * RealisticGameObjectsFactory::createEnemy(Vector2D position) {
    RealisticEnemy * e = new RealisticEnemy(position, rand() % 2 == 0);
    return e;
}

Missile * RealisticGameObjectsFactory::createMissile(Vector2D position, Vector2D velocity) {
    Missile * m = new Missile(velocity, new RealisticMovementStrategy());
    m->setPosition(position);
    return m;
}
