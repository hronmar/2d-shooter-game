#ifndef GAME_EVENT_H
#define GAME_EVENT_H

namespace events {

    enum class GameEvent {
        CANNON_SHOOT, COLLISION
    };

}

#endif /* GAME_EVENT_H */
