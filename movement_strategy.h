#ifndef MOVEMENT_STRATEGY_H
#define MOVEMENT_STRATEGY_H

#include "vector_2D.h"

using geometry::Vector2D;

namespace strategy {
    
    /* An abstract movement strategy */
    class MovementStrategy {
    public:
        virtual ~MovementStrategy() {
        }

        virtual Vector2D nextPosition(Vector2D currentPosition, Vector2D currentVelocity) = 0;
        virtual Vector2D nextVelocity(Vector2D currentVelocity) = 0;
        
        virtual MovementStrategy * copy() const = 0;
    };
    
}

#endif /* MOVEMENT_STRATEGY_H */
