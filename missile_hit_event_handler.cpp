#include "missile_hit_event_handler.h"

using namespace events;

MissileHitEventHandler::MissileHitEventHandler(EventHandler * nextInChain,
        flyweight::ResourceLoader * resourceHolder) :
EventHandler(nextInChain), m_ResourceLoader(resourceHolder) {
}

MissileHitEventHandler::MissileHitEventHandler(flyweight::ResourceLoader * resourceHolder) :
EventHandler(), m_ResourceLoader(resourceHolder) {
}

void MissileHitEventHandler::handleEvent(GameEvent event) {
    if (event == GameEvent::COLLISION) {
        m_Sound.setBuffer(m_ResourceLoader->getSound(HIT_SOUND)->get());
        m_Sound.play();
    } else if (m_Next)
        m_Next->handleEvent(event);
}

const std::string MissileHitEventHandler::HIT_SOUND = "resources/sounds/impact.wav";
