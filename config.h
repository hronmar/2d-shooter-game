#ifndef CONFIG_H
#define CONFIG_H

#include <cmath>
#include <string>

class Config {
public:

    static Config & get() {
        static Config instance;
        return instance;
    }

    std::string getAppName() const {
        return appName + " - " + (realisticMode ? "realistic" : "simple") + " mode";
    }

    int getGameHeight() const {
        return gameHeight;
    }

    int getGameWidth() const {
        return gameWidth;
    }

    float getSecondsPerFrame() const {
        return secondsPerFrame;
    }

    float getMoveStep() const {
        return moveStep;
    }

    float getRotateStep() const {
        return rotateStep;
    }

    float getMaxCannonRotation() const {
        return maxCannonRotation;
    }

    float getCannonForceStep() const {
        return cannonForceStep;
    }

    float getMaxCannonForce() const {
        return maxCannonForce;
    }

    int getMaxEnemies() const {
        return maxEnemies;
    }

    bool isRealisticMode() const {
        return realisticMode;
    }

    void setRealisticMode(bool realisticMode) {
        this->realisticMode = realisticMode;
    }

    float getGravity() const {
        return gravity;
    }

    int getCollisionLifespan() const {
        return collisionLifespan;
    }

    int getCannonCalldown() const {
        return cannonCalldown;
    }

private:

    Config() {
    }

    std::string appName = "MVC Shooter";
    int gameHeight = 600;
    int gameWidth = 600;
    float secondsPerFrame = 1.0f / 60.0f;
    float moveStep = 10.0f;
    float rotateStep = 0.1f * M_PI_2;
    float maxCannonRotation = 0.9f * M_PI_2;
    float cannonForceStep = 1.0f;
    float maxCannonForce = 15.0f;
    int maxEnemies = 10;
    bool realisticMode = true;
    float gravity = 0.1f;
    int collisionLifespan = 25;
    int cannonCalldown = 100;

public:
    Config(Config const &) = delete;
    void operator=(Config const &) = delete;
};

#endif /* CONFIG_H */
