#include "game_command.h"

#include "abstract_game_model.h"
#include "game_model.h"

using namespace command;

GameCommand::GameCommand(proxy::AbstractGameModel * m_Receiver) : m_Receiver(m_Receiver), m_Memento(nullptr) {
}

GameCommand::~GameCommand() {
    if (m_Memento != nullptr)
        delete m_Memento;
}

void GameCommand::execute() {
    if (m_Memento != nullptr) {
        //executed twice - should not happen, 
        //but to be sure we free the previous memento
        delete m_Memento;
    }

    m_Memento = m_Receiver->createMemento();
    privateExecute();
}

void GameCommand::undo() {
    if (m_Memento == nullptr) {
        //cannot undo without executing the command first
        return;
    }

    m_Receiver->setMemento(m_Memento);
    m_Memento = nullptr;
}

bool GameCommand::needsDestruction() {
    return true;
}
