#ifndef SUBJECT_H
#define SUBJECT_H

#include <set>

#include "observer.h"

namespace observer {

    class Subject {
    public:
        virtual ~Subject();

        virtual void attachObserver(Observer * observer);
        virtual void detachObserver(Observer * observer);
        virtual void notifyObservers();

    private:
        std::set<Observer *> m_Observers;
    };

}

#endif /* SUBJECT_H */
