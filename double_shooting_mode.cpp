#include "double_shooting_mode.h"

#include "cannon.h"
#include "single_shooting_mode.h"

using namespace state;
using model::Cannon;

DoubleShootingMode::DoubleShootingMode(Cannon * context) : CannonShootingMode(context) {
}

void DoubleShootingMode::shoot() {
    m_Context->aimDown();
    m_Context->addShot();
    m_Context->aimUp();
    m_Context->aimUp();
    m_Context->addShot();
    m_Context->aimDown();
}

void DoubleShootingMode::toggleMode() {
    m_Context->setShootingMode(new SingleShootingMode(m_Context));
}

std::string DoubleShootingMode::printString() const {
    return "Double shooting mode";
}

DoubleShootingMode * DoubleShootingMode::newInContext(model::Cannon * context) const {
    return new DoubleShootingMode(context);
}
