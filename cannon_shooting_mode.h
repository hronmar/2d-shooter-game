#ifndef CANNON_SHOOTING_MODE_H
#define CANNON_SHOOTING_MODE_H

#include <string>

namespace model {
    class Cannon;
}

namespace state {

    /* An abstract shooting mode */
    class CannonShootingMode {
    public:

        CannonShootingMode(model::Cannon * context) : m_Context(context) {
        }

        virtual ~CannonShootingMode() {
        }

        virtual void shoot() = 0;
        virtual void toggleMode() = 0;
        virtual std::string printString() const = 0;

        virtual CannonShootingMode * newInContext(model::Cannon * context) const = 0;
    protected:
        model::Cannon * m_Context;
    };


}

#endif /* CANNON_SHOOTING_MODE_H */
