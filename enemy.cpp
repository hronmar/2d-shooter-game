#include "enemy.h"

#include "visitor.h"

using namespace model;

void Enemy::accept(visitor::Visitor & visitor) {
    visitor.visit(*this);
}
