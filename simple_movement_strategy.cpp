#include "simple_movement_strategy.h"

using namespace strategy;
using geometry::Vector2D;

Vector2D SimpleMovementStrategy::nextPosition(Vector2D currentPosition, Vector2D currentVelocity) {
    return currentPosition.summedWith(currentVelocity);
}

Vector2D SimpleMovementStrategy::nextVelocity(Vector2D currentVelocity) {
    return currentVelocity;
}

SimpleMovementStrategy * SimpleMovementStrategy::copy() const {
    return new SimpleMovementStrategy();
}
