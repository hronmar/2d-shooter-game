#include "rectangle.h"

using namespace geometry;

Rectangle::Rectangle(Vector2D position, Vector2D size)
: m_Rect(position.asSFMLVector(), size.asSFMLVector()) {
}

bool Rectangle::contains(float x, float y) const {
    return m_Rect.contains(x, y);
}

bool Rectangle::contains(Vector2D point) const {
    return m_Rect.contains(point.asSFMLVector());
}

bool Rectangle::intersectsWith(Rectangle rect) const {
    return m_Rect.intersects(rect.m_Rect);
}

bool Rectangle::isSubsetOf(Rectangle rect) const {
    return rect.contains(m_Rect.left, m_Rect.top) 
            && rect.contains(m_Rect.left + m_Rect.width, m_Rect.top + m_Rect.height);
}
