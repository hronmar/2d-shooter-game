#ifndef SIMPLE_GAME_OBJECTS_FACTORY_H
#define SIMPLE_GAME_OBJECTS_FACTORY_H

#include "game_objects_factory.h"

#include "simple_enemy.h"

using namespace model;
using geometry::Vector2D;

namespace abstract_factory {

    class SimpleGameObjectsFactory : public GameObjectsFactory {
    public:

        Cannon * createCannon() override;
        Collision * createCollision(Vector2D position) override;
        SimpleEnemy * createEnemy(Vector2D position) override;
        Missile * createMissile(Vector2D position, Vector2D velocity) override;
    };

}

#endif /* SIMPLE_GAME_OBJECTS_FACTORY_H */
