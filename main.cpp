#include <cstdlib>

#include "game.h"
#include "config.h"

int main(int argc, char ** argv) {
    if (argc >= 2) {
        //determine game mode setting based on command line argument
        std::string mode(argv[1]);
        if (mode == "realistic")
            Config::get().setRealisticMode(true);
        else if (mode == "simple")
            Config::get().setRealisticMode(false);
    }

    Game game;

    game.run();

    return EXIT_SUCCESS;
}
