#ifndef ABSTRACT_GAME_MODEL_H
#define ABSTRACT_GAME_MODEL_H

#include <string>
#include <list>

#include "game_object.h"
#include "subject.h"
#include "game_command.h"
#include "cannon.h"
#include "event_emitter.h"

using model::GameObject;
using command::GameCommand;

namespace model {
    class Memento;
}
using model::Memento;

namespace proxy {

    class AbstractGameModel : public observer::Subject, public events::EventEmitter {
    public:
        virtual ~AbstractGameModel();

        virtual void tick() = 0;
        virtual const std::list<GameObject *> & getGameObjects() const = 0;
        virtual int getScore() const = 0;
        virtual std::string getTextInfo() const = 0;
        virtual model::Cannon * getCannon() const = 0;
        virtual void moveCannonUp() = 0;
        virtual void moveCannonDown() = 0;
        virtual void aimCannonUp() = 0;
        virtual void aimCannonDown() = 0;
        virtual void increaseForce() = 0;
        virtual void decreaseForce() = 0;
        virtual void shootCannon() = 0;
        virtual void toggleShootingMode() = 0;

        virtual bool isGameEnd() const = 0;
        virtual void reset() = 0;

        virtual void registerCommand(GameCommand * command) = 0;
        virtual void undoLastCommand() = 0;

        virtual Memento * createMemento() = 0;
        virtual void setMemento(Memento * memento) = 0;
    };

}

#endif /* ABSTRACT_GAME_MODEL_H */
