#include "subject.h"

using namespace observer;

Subject::~Subject() {
}

void Subject::attachObserver(Observer * observer) {
    m_Observers.insert(observer);
}

void Subject::detachObserver(Observer * observer) {
    m_Observers.erase(observer);
}

void Subject::notifyObservers() {
    for (Observer * o : m_Observers) {
        o->update();
    }
}
