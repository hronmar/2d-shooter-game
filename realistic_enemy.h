#ifndef REALISTIC_ENEMY_H
#define REALISTIC_ENEMY_H

#include "enemy.h"

using geometry::Vector2D;

namespace model {

    /* More advanced enemy, 
     * will change its position using periodic movement. */
    class RealisticEnemy : public Enemy {
    public:
        RealisticEnemy(Vector2D startingPos, bool movingUp = true);

        void move() override;

        Vector2D getSize() const override;

        RealisticEnemy * copy() const override;
    private:
        Vector2D m_StartingPos;
        bool m_MovingUp;
        int m_I;

        static const int MAX_I;
    };

}

#endif /* REALISTIC_ENEMY_H */
