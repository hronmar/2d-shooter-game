#ifndef GAME_COMMAND_H
#define GAME_COMMAND_H

namespace proxy {
    class AbstractGameModel;
}

namespace model {
    class Memento;
}

namespace command {

    /* Abstract command */
    class GameCommand {
    public:

        GameCommand(proxy::AbstractGameModel * m_Receiver);

        virtual ~GameCommand();

        virtual void execute();

        virtual void undo();
        
        virtual bool needsDestruction();

    protected:
        proxy::AbstractGameModel * m_Receiver;

        virtual void privateExecute() = 0;

    private:
        model::Memento * m_Memento;
    };

}

#endif /* GAME_COMMAND_H */
