#include "game_window.h"

#include "config.h"
#include "cannon_shoot_event_handler.h"
#include "composite_event_handler.h"
#include "missile_hit_event_handler.h"
#include "enemy_die_event_handler.h"

using namespace view;
using proxy::AbstractGameModel;

GameWindow::GameWindow(controller::GameController * controller) :
RenderWindow(sf::VideoMode(Config::get().getGameWidth(), Config::get().getGameHeight()),
Config::get().getAppName(), sf::Style::Titlebar | sf::Style::Close),
m_Controller(controller), m_Model(nullptr),
m_ResourceLoader(), m_Drawer(&m_ResourceLoader) {
    setPosition(sf::Vector2i((sf::VideoMode::getDesktopMode().width - Config::get().getGameWidth()) / 2,
            (sf::VideoMode::getDesktopMode().height - Config::get().getGameHeight()) / 2));
    setModel(controller->getModel());

    m_Drawer.setRenderTarget(this);
    render();

    events::CannonShootEventHandler * h1 = new events::CannonShootEventHandler(&m_ResourceLoader);
    events::CompositeEventHandler * h2 = new events::CompositeEventHandler();
    h2->addChild(new events::MissileHitEventHandler(&m_ResourceLoader));
    h2->addChild(new events::EnemyDieEventHandler(&m_ResourceLoader));
    h1->setNext(h2);
    m_Model->setHandler(h1);
}

AbstractGameModel * GameWindow::getModel() const {
    return m_Model;
}

void GameWindow::setModel(AbstractGameModel * model) {
    if (model == m_Model)
        return;

    if (m_Model != nullptr)
        m_Model->detachObserver(this);

    m_Model = model;
    m_Model->attachObserver(this);
}

void GameWindow::processEvents() {
    sf::Event event;
    while (pollEvent(event)) {
        m_Controller->processEvent(event);
        if (event.type == sf::Event::Closed)
            close();
    }
}

void GameWindow::update() {
    render();
}

void GameWindow::render() {
    clear(sf::Color::White);

    if (m_Model->isGameEnd())
        displayEndGameScreen();
    else
        renderScene();

    display();
}

void GameWindow::renderScene() {
    for (auto o : m_Model->getGameObjects())
        o->accept(m_Drawer);

    m_Drawer.displayInfoText(m_Model->getTextInfo());
}

void GameWindow::displayEndGameScreen() {
    int score = m_Model->getScore();
    int shotsFired = m_Model->getCannon()->getShotsFired();
    m_Drawer.displayEndGameText("Game over. Your score is " + std::to_string(score) + ".\n"
            + "You fired " + std::to_string(shotsFired) + " shots.\n"
            + "Your accuracy is " + std::to_string((int) std::round((double) score / shotsFired * 100.)) + "%\n\n"
            + "Press any key to play again.");
}
