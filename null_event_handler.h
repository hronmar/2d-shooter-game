#ifndef NULL_EVENT_HANDLER_H
#define NULL_EVENT_HANDLER_H

#include "event_handler.h"

namespace events {

    class NullEventHandler : public EventHandler {
    public:

        void handleEvent(GameEvent event) override {
            //do nothing
        }

    };

}

#endif /* NULL_EVENT_HANDLER_H */
