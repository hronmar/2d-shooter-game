#include "simple_enemy.h"

#include "visitor.h"

using namespace model;

void SimpleEnemy::move() {
    //do nothing - simple enemies are stationary
}

geometry::Vector2D SimpleEnemy::getSize() const {
    return geometry::Vector2D(30, 30);
}

SimpleEnemy * SimpleEnemy::copy() const {
    return new SimpleEnemy(*this);
}
