#include "vector_2D.h"

#include <cmath>

#include "rectangle.h"
#include "utility.h"

using namespace geometry;

Vector2D::Vector2D() : Vector2D(0, 0) {
}

Vector2D::Vector2D(float x, float y) : m_Vector(x, y) {
}

Vector2D::Vector2D(sf::Vector2f vector) : m_Vector(vector) {
}

const Vector2D Vector2D::ZERO(0, 0);
const Vector2D Vector2D::UNIT_UP(0, -1);
const Vector2D Vector2D::UNIT_DOWN(0, 1);
const Vector2D Vector2D::UNIT_LEFT(-1, 0);
const Vector2D Vector2D::UNIT_RIGHT(1, 0);

float Vector2D::getX() const {
    return m_Vector.x;
}

void Vector2D::setX(float x) {
    m_Vector.x = x;
}

float Vector2D::getY() const {
    return m_Vector.y;
}

void Vector2D::setY(float y) {
    m_Vector.y = y;
}

sf::Vector2f Vector2D::asSFMLVector() const {
    return m_Vector;
}

void Vector2D::translateBy(Vector2D vector) {
    m_Vector += vector.asSFMLVector();
}

Vector2D Vector2D::summedWith(Vector2D vector) const {
    Vector2D res(*this);
    res.translateBy(vector);
    return res;
}

void Vector2D::multiplyBy(float k) {
    m_Vector *= k;
}

Vector2D Vector2D::multipliedBy(float k) const {
    Vector2D res(*this);
    res.multiplyBy(k);
    return res;
}

Vector2D Vector2D::negated() const {
    return multipliedBy(-1);
}

float Vector2D::getEuclideanNorm() const {
    return sqrtf(m_Vector.x * m_Vector.x + m_Vector.y * m_Vector.y);
}

void Vector2D::normalize() {
    multiplyBy(1 / getEuclideanNorm());
}

Vector2D Vector2D::normalized() const {
    Vector2D res(*this);
    res.normalize();
    return res;
}

float Vector2D::distanceTo(Vector2D vector) const {
    return summedWith(vector.negated()).getEuclideanNorm();
}

bool Vector2D::equalTo(Vector2D vector) const {
    return Utility::cmpf(getX(), vector.getX()) && Utility::cmpf(getY(), vector.getY());
}

void Vector2D::rotateBy(float angle) {
    float cs, sn;
    sf::Vector2f old = m_Vector;
    sincosf(angle, &sn, &cs);
    m_Vector.x = cs * old.x - sn * old.y;
    m_Vector.y = sn * old.x + cs * old.y;
}

Vector2D Vector2D::rotatedBy(float angle) {
    Vector2D res(*this);
    res.rotateBy(angle);
    return res;
}

Rectangle Vector2D::rectangleTo(Vector2D other) const {
    Vector2D position(std::min(getX(), other.getX()), std::min(getY(), other.getY()));
    Vector2D size(fabsf(other.getX() - getX()), fabsf(other.getY() - getY()));
    return Rectangle(position, size);
}
