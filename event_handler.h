#ifndef EVENT_HANDLER_H
#define EVENT_HANDLER_H

#include "game_event.h"

namespace events {

    class EventHandler {
    public:

        EventHandler(EventHandler * nextInChain) : m_Next(nextInChain) {
        }

        EventHandler() : EventHandler(nullptr) {
        }

        virtual ~EventHandler() {
            if (m_Next != nullptr)
                delete m_Next;
        }
        
        virtual void handleEvent(GameEvent event) = 0;

        void setNext(EventHandler * next) {
            m_Next = next;
        }

    protected:
        EventHandler * m_Next;
    };

}

#endif /* EVENT_HANDLER_H */
