#include "enemy_die_event_handler.h"

#include <SFML/Audio.hpp>

using namespace events;

EnemyDieEventHandler::EnemyDieEventHandler(EventHandler * nextInChain,
        flyweight::ResourceLoader * resourceLoader) :
EventHandler(nextInChain), m_ResourceLoader(resourceLoader) {
}

EnemyDieEventHandler::EnemyDieEventHandler(flyweight::ResourceLoader * resourceLoader) :
EventHandler(), m_ResourceLoader(resourceLoader) {
}

void EnemyDieEventHandler::handleEvent(GameEvent event) {
    if (event == GameEvent::COLLISION) {
        m_Sound.setBuffer(m_ResourceLoader->getSound(ENEMY_DIE_SOUND)->get());
        m_Sound.play();
    } else if (m_Next)
        m_Next->handleEvent(event);
}

const std::string EnemyDieEventHandler::ENEMY_DIE_SOUND = "resources/sounds/enemy_die.wav";
