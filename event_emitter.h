#ifndef EVENT_EMITTER_H
#define EVENT_EMITTER_H

#include "game_event.h"
#include "event_handler.h"

namespace events {

    class EventEmitter {
    public:
        EventEmitter();
        virtual ~EventEmitter();

        virtual EventHandler * getHandler() const;
        virtual void setHandler(EventHandler * handler);

    protected:
        void emitEvent(GameEvent event);

        EventHandler * m_Handler;
    };

}

#endif /* EVENT_EMITTER_H */
