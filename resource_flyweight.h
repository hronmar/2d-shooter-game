#ifndef RESOURCE_FLYWEIGHT_H
#define RESOURCE_FLYWEIGHT_H

#include <SFML/Graphics/Texture.hpp>
#include <SFML/Graphics/Font.hpp>
#include <SFML/Audio/SoundBuffer.hpp>

namespace flyweight {

    /* An abstract resource holder. This is the flyweight class. */
    template <typename T>
    class ResourceFlyweight {
    public:

        ResourceFlyweight(std::string pathToResource) : m_Path(pathToResource) {
            m_Resource.loadFromFile(pathToResource);
        }

        virtual ~ResourceFlyweight() {
        }

        std::string getPath() const {
            return m_Path;
        }

        const T & get() const {
            return m_Resource;
        }

        T & get() {
            return m_Resource;
        }

    protected:
        std::string m_Path;
        T m_Resource;
    };

    typedef ResourceFlyweight<sf::Texture> TextureFlyweight;
    typedef ResourceFlyweight<sf::Font> FontFlyweight;
    typedef ResourceFlyweight<sf::SoundBuffer> SoundFlyweight;
}

#endif /* RESOURCE_FLYWEIGHT_H */
